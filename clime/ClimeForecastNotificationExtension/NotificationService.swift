//
//  NotificationService.swift
//  ClimeForecastNotificationExtension
//
//  Created by Tunde Adegoroye on 29/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UserNotifications


class NotificationService: UNNotificationServiceExtension {

    
    private let clearMessages = [
    
        "hot": [
            "Get out the shades, it’s going to be a sunny day.",
            "Gah lee it’s hot outside today right?",
            "Do you like Frank Ocean? Go and enjoy his music in the sun.",
            "Ayeeee you hear about the good news? The sun is out today!",
            "Today might a good day to consider sun cream.",
            "It’s time for you to go an slay in the sun."
        ],
        "cold": [
            "Don’t be fooled by the sun, winter is coming.",
            "Yeah the sun’s out today, but trust me it’s kinda cold.",
            "It’s sunny but cold…I know life isn’t fair right."
        ],
        "mild": [
            "Look outside the window you see that orange ball? That’s the sun."
        ]
    ]
    
    private let clearNightMessages = [
        
        "hot": [
            "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
            "Turn on all the fans it’s going to be a hot night tonight.",
            "One fan isn’t going to be enough, you might melt."
        ],
        "cold": [
            "Get some hot coco to warm you up tonight.",
            "Your bed might feel like a slab of ice for the first 5 seconds, it’s that cold tonight.",
            "Can you see cold air?, don’t worry you might survive the night…"
        ],
        "mild": [
            "It’s going to be a clear night, wear your best outfit p.s. pj’s count too.",
            "Get a nice view of the stars, it’s going to be a clear night tonight.",
            "It’s a clear night cuddle up with someone, you cats or your pillow…",
            "It’s going to be a clear night, btw you saw them stars?"
        ]
    ]
    
    private let cloudyNightMessages = [
        
        "hot": [
            "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
            "Turn on all the fans it’s going to be a hot night tonight.",
            "One fan isn’t going to be enough, you might melt."
        ],
        "cold": [
            "Get some hot coco to warm you up tonight.",
            "Your bed might feel like a slab of ice for the first 5 seconds, it’s that cold tonight.",
            "Can you see cold air?, don’t worry you might survive the night…"
        ],
        "mild": [
            "It’s going to be a cloudy night, wear your best outfit p.s. pj’s count too.",
            "Get a nice view of the stars, it’s going to be a cloudy night tonight.",
            "It’s a clear night cuddle up with someone, you cats or your pillow…",
            "It’s going to be a cloudy night, btw you saw them stars?"
        ]
    ]
    
    private let partlyCloudyDayMessages = [
        
        "hot": [
            "Although the clouds are out it’s still going to be a hot day."
        ],
        "cold": [
            "Don’t let the sun fool you it’s still going to be cold."
        ],
        "mild": [
            "The sun and clouds are both fighting for your attention."
        ]
    ]
    
    private let cloudyMessages = [
        
        "hot": [
            "Although the clouds are out it’s still going to be a hot day.",
            "It’s going to look grey today because it’s cloudy…"
        ],
        "cold": [
            "So it’s cloudy and cold… great."
        ],
        "mild": [
            "The sun’s on its break, today you’ll have to deal with the clouds.",
            "It’s miserable but remember you’re a star. Cheesy but what you gonna do?",
            "Oh wait the clouds are out today…"
        ]
    ]
    
    private let rainMessages = [
        
        "hot": [
            "So it’s hot and raining… I wonder if the water burns?",
            "Its hot and raining I just don’t even know anymore."
        ],
        "cold": [
            "Today would be a valid day to stay in all day and play video games.",
            "It’s cold and raining… (Hey Arthur fist)."
        ],
        "mild": [
            "Listening to Drake in this rain is only going to make this day sadder",
            "It’s raining sooo, do you want to just binge on YouTube videos all day?",
            "It’s raining… Who needs the outdoors anyways"
        ]
    ]
    
    private let sleetMessages = [
        
        "hot": [
            "Watch out for that sleet, if one gets in your eye it could be game over.",
            "That’s not snow it’s it evil sibling sleet."
        ],
        "cold": [
            "Watch out for that sleet, if one gets in your eye it could be game over.",
            "That’s not snow it’s it evil sibling sleet."
        ],
        "mild": [
            "Watch out for that sleet, if one gets in your eye it could be game over.",
            "That’s not snow it’s it evil sibling sleet."
        ]
    ]
    
    private let snowMessages = [
        
        "hot": [
            "How are your snowball sniping skills?",
            "IT’S SNOWINGGGGGGG!",
            "Go on outside and throw a couple of snowballs.",
            "We’ve been blessed with snow today.",
            "You see that jumper you’ve been avoiding for months… It’s time."
        ],
        "cold": [
            "How are your snowball sniping skills?",
            "IT’S SNOWINGGGGGGG!",
            "Go on outside and throw a couple of snowballs.",
            "We’ve been blessed with snow today.",
            "You see that jumper you’ve been avoiding for months… It’s time."
        ],
        "mild": [
            "How are your snowball sniping skills?",
            "IT’S SNOWINGGGGGGG!",
            "Go on outside and throw a couple of snowballs.",
            "We’ve been blessed with snow today.",
            "You see that jumper you’ve been avoiding for months… It’s time."
        ]
    ]
    
    private let windMessages = [
        
        "hot": [
            "If you’re not already flying in the air, did you know it’s going to be windy?",
            "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
            "It’s might be chilly because of the wind."
        ],
        "cold": [
            "If you’re not already flying in the air, did you know it’s going to be windy?",
            "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
            "It’s might be chilly because of the wind."
        ],
        "mild": [
            "If you’re not already flying in the air, did you know it’s going to be windy?",
            "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
            "It’s might be chilly because of the wind."
        ]
    ]
    
    private let fogMessages = [
        
        "hot": [
            "It’s going to be foggy, please don’t run over that cat…",
            "It may look like a ghost town due to the fog."
        ],
        "cold": [
            "It’s going to be foggy, please don’t run over that cat…",
            "It may look like a ghost town due to the fog."
        ],
        "mild": [
            "It’s going to be foggy, please don’t run over that cat…",
            "It may look like a ghost town due to the fog."
        ]
    ]
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var remoteContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        // Copy the content
        remoteContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let remoteContent = remoteContent {
            
            let userInfo = remoteContent.userInfo as NSDictionary
            
            if let icon = userInfo.value(forKeyPath: "aps.icon"),
               let temp = userInfo.value(forKeyPath: "aps.temp"){
               
            
               // Replace the notification body
               remoteContent.body = getRandomMessage(icon: icon as! String, temp: temp as! Int)
                
                do {
                    // Get the image from the bundle
                    let fileUrl = Bundle.main.url(forResource: "notification-\(icon as! String)", withExtension: "png")!
                    let attachment = try UNNotificationAttachment(identifier : "image", url: fileUrl, options: nil)
                    remoteContent.attachments = [attachment]
                } catch {}
                
            }
            
            contentHandler(remoteContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let remoteContent =  remoteContent {
            contentHandler(remoteContent)
        }
    }
    
    private func getRandomMessage(icon: String, temp: Int) -> String {
    
        switch icon {
        case "clear-day":
            
            
            if temp >= 80 {
                return generateMessage(messages: clearMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: clearMessages["cold"]!)
            } else {
               return generateMessage(messages: clearMessages["mild"]!)
            }

            break;
        case "clear-night":
            
            if temp >= 80 {
                return generateMessage(messages: clearNightMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: clearNightMessages["cold"]!)
            } else {
                return generateMessage(messages: clearNightMessages["mild"]!)
            }

            
            break;
        case "partly-cloudy-night":
            
            if temp >= 80 {
                return generateMessage(messages: cloudyNightMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: cloudyNightMessages["cold"]!)
            } else {
                return generateMessage(messages: cloudyNightMessages["mild"]!)
            }

            
            break;
        case "partly-cloudy-day":
            
            if temp >= 80 {
                return generateMessage(messages: partlyCloudyDayMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: partlyCloudyDayMessages["cold"]!)
            } else {
                return generateMessage(messages: partlyCloudyDayMessages["mild"]!)
            }

            
            break;
        case "cloudy":
            
            if temp >= 80 {
                return generateMessage(messages: cloudyMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: cloudyMessages["cold"]!)
            } else {
                return generateMessage(messages: cloudyMessages["mild"]!)
            }

            
            break;
        case "rain":
            
            if temp >= 80 {
                return generateMessage(messages: rainMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: rainMessages["cold"]!)
            } else {
                return generateMessage(messages: rainMessages["mild"]!)
            }

            break;
        case "sleet":
            
            if temp >= 80 {
                return generateMessage(messages: sleetMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: sleetMessages["cold"]!)
            } else {
                return generateMessage(messages: sleetMessages["mild"]!)
            }

            
            break;
        case "snow":
            
            if temp >= 80 {
                return generateMessage(messages: snowMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: snowMessages["cold"]!)
            } else {
                return generateMessage(messages: snowMessages["mild"]!)
            }

            
            break;
        case "wind":
            
            if temp >= 80 {
                return generateMessage(messages: windMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: windMessages["cold"]!)
            } else {
                return generateMessage(messages: windMessages["mild"]!)
            }

            
            break;
        case "fog":
            
            if temp >= 80 {
                return generateMessage(messages: fogMessages["hot"]!)
            } else if temp <= 40 {
                return generateMessage(messages: fogMessages["cold"]!)
            } else {
                return generateMessage(messages: fogMessages["mild"]!)
            }

            
            break;
        default:
            break;
            
        }
        
        return ""
    }
    
    private func generateMessage(messages: [String]) -> String {
        
        let index = Int(arc4random_uniform(UInt32(messages.count)))
        
        return messages[index]
    }

}
