//
//  Location.swift
//  clime
//
//  Created by Tunde Adegoroye on 29/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//


import RealmSwift
import GooglePlaces
import PromiseKit
import LatLongToTimezone
import Parse

class Locations: Object {
    
    dynamic var id = ""
    dynamic var name = ""
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    dynamic var createdDate = Date()
    dynamic var isActive = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


class Location {
    
    
    private let realm = try! Realm()
    private var placesClient: GMSPlacesClient!
    private var googleSecretKey = "AIzaSyDThQHzkzWTuxoTmXaWWgFaLh2Mb4FwXvU"
    
    init() {
        placesClient = GMSPlacesClient.shared()
    }
    
    /*
     * Function to get current location
     */
    
    public func getCurrentPlace() -> Promise<GMSPlace> {
        
        return Promise { success, fail in
            
            placesClient.currentPlace { placesList, error in
                
                if let _ = error {} else {
                    
                    if let places = placesList {
                        
                        if let place = places.likelihoods.first?.place {
                            
                            success(place)
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    /*
     * Function to get the user's current city
     */
    
    public func getCityName(lat: Double, long: Double) -> Promise<String> {
        
        return Promise { success, fail in
            
            let geoCoder = CLGeocoder()
            let coordinates = CLLocation(latitude: lat, longitude: long)
            geoCoder.reverseGeocodeLocation(coordinates) { (placemarks, error) in
                
                if let error = error {
                    fail(error)
                    
                } else {
                    
                    var text = ""
                    
                    let place = placemarks as [CLPlacemark]!
                    
                    if let city = place?[0].addressDictionary?["City"] as! String? {
                    
                        text += "\(city)"
                    }
                    
                    success(text)
                    
                }
                
            }
            
        }
        
    }
    
    
    /**
     * CRUD Methods
     */
    
    /*
     * Function to get all locations
     */
    
    public func getLocations() -> Promise<Results<Locations>> {
        
        return Promise { success, fail in
            
            success(realm.objects(Locations.self).sorted(byProperty: "createdDate", ascending: false))
            
        }
        
    }
    
    /*
     * Function to get active current location
     */
    
    public func getActiveLocation() -> Results<Locations> {
        
        let predicate = NSPredicate(format: "isActive = %@", NSNumber(value: true))
        return realm.objects(Locations.self).filter(predicate)
        
    }
    
    /*
     * Function to set active location
     */
    
    public func setActiveLocation(location: Locations) -> Promise<Void> {
        
        return Promise { success, fail in
            
            do {
                
                try! realm.write {
                    realm.create(Locations.self, value: ["id": location.id, "isActive": true], update: true)
                    success()
                }
            }
        }
        
    }
    
    
    /*
     * Function to set most recent location active
     */
    
    public func setMostRecentActive() -> Promise<Void> {
        
        return Promise { success, fail in
            
            do {
                
                if let location = realm.objects(Locations.self).sorted(byProperty: "createdDate", ascending: false).first {
                
                    try! realm.write {
                        realm.create(Locations.self, value: ["id": location.id, "isActive": true], update: true)
                        success()
                    }
                
                }
                
            }
        }
        
    }
    
    /*
     * Function to delete a location
     */
    
    public func deleteLocation(location: Locations) -> Promise<Void> {
        
        return Promise { success, fail in
            
            do {
                
                let predicate = NSPredicate(format: "id = %@", location.id)
                
                try! realm.write {
                    realm.delete(realm.objects(Locations.self).filter(predicate))
                    success()
                }
            }
        }
        
    }
    
    /*
     * Function to save a location
     */
    
    public func saveCoordinates(place: GMSPlace, active: Bool){
        
        let newLocation = Locations()
        newLocation.id = String.generateRandomId()
        newLocation.name = place.formattedAddress!
        newLocation.latitude = place.coordinate.latitude
        newLocation.longitude = place.coordinate.longitude
        newLocation.isActive = active
        newLocation.createdDate = Date()
        
        do {
            
            try! realm.write {
                realm.add(newLocation, update: true)
            }
        }
        
    }
    
    /*
     * Function to reset all location
     */
    
    public func resetAllLocations() -> Promise<Void>{
        
        return Promise { success, fail in
            
            do {
                
                try! realm.write {
                    
                    realm.objects(Locations.self).setValue(false, forKey: "isActive")
                    success()
                }
                
            }
        }
    }
    
    /*
     * Function to save time zone in parse db
     */
    
    public func saveNewTimeZone(long: Double, lat: Double) -> Promise<Void>{
        
        return Promise { success, fail in
            
            
            let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let timeZone = TimezoneMapper.latLngToTimezone(location)
            
            // Get the timezone from the lat and long
            if let identifier = timeZone?.identifier {
                
                // Update the users timezone field
                let installation = PFInstallation.current()
                installation?.setObject(identifier, forKey: "timeZoneIdentifier")
                installation?.setObject(lat, forKey: "latitude")
                installation?.setObject(long, forKey: "longitude")
                installation?.saveInBackground(block: { result, error in
                    
                    if result {

                        do {
                            try installation?.fetch()
                        }catch{}
                        success()
                    }
                })
                
            }
            
        }
    }
    
    /*
     * Function to save a new city in parse db
     */
    public func saveNewCity(long: Double, lat: Double) -> Promise<Void> {
        
        return Promise { success, fail in
        
            getCityName(lat: lat, long: long).then { city -> Void in
            
                let installation =  PFInstallation.current()
                installation?.setObject(city, forKey: "timeZoneCity")
                installation?.setObject(lat, forKey: "latitude")
                installation?.setObject(long, forKey: "longitude")
                installation?.saveInBackground(block: { result, error in
                    
                    if result {
                        
                        do {
                            try installation?.fetch()
                        } catch {}
                    }
                })
            }
            
        }
    }
    
}

extension String {
    
    static func generateRandomId(length: Int = 7) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
