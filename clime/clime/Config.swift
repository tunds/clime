//
//  Config.swift
//  clime
//
//  Created by Tunde Adegoroye on 28/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import RealmSwift
import Parse


enum Emotions: Int {
    case happy = 0, sad = 1, angry = 2
}

class UserDefault: Object {
    
    dynamic var id = 0
    dynamic var isFirstTimeLaunch = true
    dynamic var isFirstTimeShare = true
    dynamic var isFirstTimeDelete = true
    dynamic var hasSeenShake = false
    dynamic var isShakeable = false
    dynamic var isNotified = false
    dynamic var isCelsius = true
    
    override static func primaryKey() -> String? {
        return "id"
    }

}

class Config {
    
   private let realm = try! Realm()
    
    /*
     * Function to set the onboarding status
     */
    
   public func seenOnboarding() {
        
        let userDefault = UserDefault()
        userDefault.id = 0
        userDefault.isFirstTimeLaunch = false
        
        do {
        
            try! realm.write {
                realm.add(userDefault, update: true)
            }
        }
    }
    
    /*
     * Function to set the share status
     */
    
    public func seenShare() {
        
        do {
            
            try! realm.write {
                realm.create(UserDefault.self, value: ["id": 0, "isFirstTimeShare": false], update: true)
            }
        }
    }
    
    /*
     * Function to set the delete status
     */
    
    public func seenDelete() {
        
        do {
            
            try! realm.write {
                realm.create(UserDefault.self, value: ["id": 0, "isFirstTimeDelete": false], update: true)
            }
        }
    }
    
    /*
     * Function to toggle the shakeable status - You can only make API calls by shaking the device
     */
    
    public func toggleShakeable(withStatus: Bool) {
        
        do {
            
            try! realm.write {
                realm.create(UserDefault.self, value: ["id": 0, "isShakeable": withStatus], update: true)
            }
        }
    }
    
    /*
     * Function to set the shakeable seen
     */
    
    public func setShakeableSeen() {
        
        do {
            
            try! realm.write {
                realm.create(UserDefault.self, value: ["id": 0, "hasSeenShake": true], update: true)
            }
        }
    }
    
    /*
     * Function to toggle the weather type
     */
    
    public func toggleTempType(withStatus: Bool) {
        
        do {
            
            try! realm.write {
                realm.create(UserDefault.self, value: ["id": 0, "isCelsius": withStatus], update: true)
            }
        }
    }
    
    /*
     * Function to toggle weather to send notifications
     */
    
    public func toggleNotifications(withValue: Bool) {
        
        let installation =  PFInstallation.current()
        installation?.setObject(withValue, forKey: "notificationsActive")
        installation?.fetchInBackground(block: { result, error in
            
        })
        
        
        
        installation?.saveInBackground(block: { result, error in
            
            if result {
                
                do {
                    try installation?.fetch()
                } catch {}
            }
        })
       
    }
    
    /*
     * Function to get the shakeable status
     */
    
    public func verfiyShakeable() -> Bool {
        
        return realm.objects(UserDefault.self).first?.value(forKey: "isShakeable") as! Bool
        
    }
    
    /*
     * Function to get the shakeable status
     */
    
    public func verfiyShakeableSeen() -> Bool {
        
        return realm.objects(UserDefault.self).first?.value(forKey: "hasSeenShake") as! Bool
        
    }
    
    /*
     * Function to get the shakeable status
     */
    
    public func verfiyNotifications() -> Bool {
        
        
        if let status = PFInstallation.current()?.object(forKey: "notificationsActive") {
            
            return status as! Bool
        }
        
        return false
        
    }
    
    /*
     * Function to get the onboarding status
     */
    
    public func verfiyTempType() -> Bool {
        
        return realm.objects(UserDefault.self).first?.value(forKey: "isCelsius") as! Bool
        
    }
    
    /*
     * Function to get the onboarding status
     */
    
    public func verfiySeenOnboarding() -> Bool {
        
        if realm.objects(UserDefault.self).first?.value(forKey: "isFirstTimeLaunch") != nil {
            
            return true
        }
        
        return false
        
    }
    
    /*
     * Function to get the share status
     */
    
    public func verfiySeenShare() -> Bool {
        
        if realm.objects(UserDefault.self).first?.value(forKey: "isFirstTimeShare") as! Bool == true {
            
            return true
        }
        
        return false
        
    }
    
    /*
     * Function to get the delete status
     */
    
    public func verfiySeenDelete() -> Bool {
        
        if realm.objects(UserDefault.self).first?.value(forKey: "isFirstTimeDelete") as! Bool == true {
            
            return true
        }
        
        return false
        
    }
}
