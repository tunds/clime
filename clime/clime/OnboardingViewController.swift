//
//  OnboardingViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 13/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Foundation
import EAIntroView
import pop
import GooglePlaces
import PromiseKit

class OnboardingViewController: UIViewController, EAIntroDelegate, CLLocationManagerDelegate {
    
    // Location objects
    private let locationManager = CLLocationManager()
    private let authorizationStatus = CLLocationManager.authorizationStatus()
    
    // Setting the configuration
    private let config = Config()
    
    // Pages for the onboarding
    private var customizeScreen = NSObject(), peakScreen = NSObject(), shareScreen = NSObject()
    
    // Customize controls
    private var customizeSelectVw = UIView(), customizeActiveVw = UIView(), customizeDeleteVw = UIView(), customizeSlideVw = UIView()
    
    // Peak controls
    private var peakSelectVw = UIView(), peakSlideDownVw = UIView(), peakDataVw = UIView(), peakContainerVw1 = UIView(), peakContainerVw2 = UIView(), peakContainerVw3 = UIView(), peakContainerVw4 = UIView()
    
    // Share controls
    private var shareSlideVw = UIView(), shareOverlayVw = UIView(), sharePopOverVw = UIView(), shareContainerVw1 = UIView(), shareContainerVw2 = UIView(), shareContainerVw3 = UIView(), shareContainerVw4 = UIView(), shareContainerVw5 = UIView()
    
    // Onboarding screen
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the delegate
        locationManager.delegate = self
        
        // If the user hasn't already given persmission for their location
        switch authorizationStatus {
            
        case .denied, .notDetermined, .restricted:
            locationManager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
        
        
        configOnboarding()
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    /**
     * Function to configure the onboarding screens
     *
     */
    
    func configOnboarding() {
        
        // Get the layout files for the onboarding screens
        customizeScreen = EAIntroPage(customViewFromNibNamed: "Onboarding-Customize")
        peakScreen = EAIntroPage(customViewFromNibNamed: "Onboarding-Peak")
        shareScreen = EAIntroPage(customViewFromNibNamed: "Onboarding-Share")
        
        // Configure screen & skip button
        let onboardingScreen = EAIntroView(frame: self.view.bounds, andPages: [customizeScreen, peakScreen, shareScreen])
        onboardingScreen?.skipButton = createDismiss(title: "Skip", boldIt: false)
        onboardingScreen?.skipButton.isEnabled = true
        onboardingScreen?.skipButton.alpha = 1
        onboardingScreen?.skipButtonY = self.view.bounds.height - 45
        
        // Prevent swiping last screen to close
        onboardingScreen?.swipeToExit = false
        
        // Configure the delegate
        onboardingScreen?.delegate = self
        
        // Attach the onboarding view to the screen
        onboardingScreen?.show(in: self.view)
        
    }
    
    /**
     * Function to get a generated button
     *
     */
    
    func createDismiss(title: String, boldIt: Bool) -> UIButton {
        
        // Create button set the text and size
        let dismissButton = UIButton()
        dismissButton.setTitle(title, for: .normal)
        dismissButton.frame = CGRect(x: 0, y: 0, width: 25, height: 16)
        
        // Set the font depending on the status
        if boldIt {
            dismissButton.titleLabel!.font = UIFont(name: "Lato-Bold", size: 16)
        } else {
            dismissButton.titleLabel!.font = UIFont(name: "Lato-Light", size: 16)
        }
        
        return dismissButton
    }
    
    // MARK: ONBOARDING DELEGATE
    func intro(_ introView: EAIntroView!, pageAppeared page: EAIntroPage!, with pageIndex: UInt) {
        
        switch pageIndex {
            
        case 0:
            
            resetPeakAnimation()
            
            // Get the customize control
            customizeSelectVw = page.pageView.viewWithTag(1)!
            customizeActiveVw = page.pageView.viewWithTag(2)!
            customizeDeleteVw = page.pageView.viewWithTag(3)!
            customizeSlideVw  = page.pageView.viewWithTag(4)!
            
            // Configure the skip button, the fade will change the text
            introView.skipButton.titleLabel?.fadeTransition(duration: 0.3)
            introView.skipButton.setTitle("Skip", for: .normal)
            introView.skipButton.titleLabel?.font = UIFont(name: "Lato-Light", size: 15)
            
            // Perform animations
            customizeAnimation()

            break;
        case 1:
            
            resetShareAnimation()
            resetCustomizeAnimation()
            
            peakSlideDownVw = page.pageView.viewWithTag(1)!
            
            // Peak gradient controls
            peakSelectVw = page.pageView.viewWithTag(2)!
            peakDataVw = page.pageView.viewWithTag(3)!
            
            // Other gradient control
            peakContainerVw1 = page.pageView.viewWithTag(4)!
            peakContainerVw2 = page.pageView.viewWithTag(5)!
            peakContainerVw3 = page.pageView.viewWithTag(6)!
            peakContainerVw4 = page.pageView.viewWithTag(7)!
            
            // Add gradients to the controls
            peakSelectVw.layer.insertSublayer(Gradients().clearDay(bounds: peakSelectVw.bounds), at: 0)
            peakDataVw.layer.insertSublayer(Gradients().clearNight(bounds: peakDataVw.bounds), at: 0)
            peakContainerVw1.layer.insertSublayer(Gradients().clearDay(bounds: peakContainerVw1.bounds), at: 0)
            peakContainerVw2.layer.insertSublayer(Gradients().clearDay(bounds: peakContainerVw2.bounds), at: 0)
            peakContainerVw3.layer.insertSublayer(Gradients().clearNight(bounds: peakContainerVw3.bounds), at: 0)
            peakContainerVw4.layer.insertSublayer(Gradients().rain(bounds: peakContainerVw4.bounds), at: 0)
            
            
            introView.skipButton.titleLabel?.fadeTransition(duration: 0.3)
            introView.skipButton.setTitle("Skip", for: .normal)
            introView.skipButton.titleLabel?.font = UIFont(name: "Lato-Light", size: 15)
            
            
            peakAnimation()
            
            break;
        case 2:
            
            resetPeakAnimation()
            
            shareSlideVw = page.pageView.viewWithTag(1)!
            shareOverlayVw = page.pageView.viewWithTag(2)!
            sharePopOverVw = page.pageView.viewWithTag(3)!
            
            shareContainerVw1 = page.pageView.viewWithTag(4)!
            shareContainerVw2 = page.pageView.viewWithTag(5)!
            shareContainerVw3 = page.pageView.viewWithTag(6)!
            shareContainerVw4 = page.pageView.viewWithTag(7)!
            shareContainerVw5 = page.pageView.viewWithTag(8)!
            
            
            shareSlideVw.layer.insertSublayer(Gradients().clearDay(bounds: shareSlideVw.bounds), at: 0)
            shareContainerVw1.layer.insertSublayer(Gradients().clearDay(bounds: shareContainerVw1.bounds), at: 0)
            shareContainerVw2.layer.insertSublayer(Gradients().clearNight(bounds: shareContainerVw2.bounds), at: 0)
            shareContainerVw3.layer.insertSublayer(Gradients().clearNight(bounds: shareContainerVw3.bounds), at: 0)
            shareContainerVw4.layer.insertSublayer(Gradients().rain(bounds: shareContainerVw4.bounds), at: 0)
            shareContainerVw5.layer.insertSublayer(Gradients().clearDay(bounds: shareContainerVw5.bounds), at: 0)
            
            
            introView.skipButton.titleLabel?.fadeTransition(duration: 0.3)
            introView.skipButton.setTitle("Done", for: .normal)
            introView.skipButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 15)
        
            shareAnimation()
            
            break;
        default:
            break;
            
        }
    }
    
    
    
    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        
        // Set the onboarding status & go to the main screen
        self.config.seenOnboarding()
        self.performSegue(withIdentifier: "weatherSegue", sender: nil)
        
    }
    
    
    /*
     * Customize animations
     */
    
    func customizeAnimation() {
        
        // 0.5 second delay to squeeze the cell in and out
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            
            // Squeeze the cell in
            let squeeze = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
            squeeze?.fromValue = NSValue(cgPoint: CGPoint(x: 0.9, y: 0.9))
            squeeze?.toValue = NSValue(cgPoint: CGPoint(x: 1, y: 1))
            squeeze?.springBounciness = 1
            squeeze?.dynamicsFriction = 9

            // Fade the cell in
            let show = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            show?.fromValue = 0
            show?.toValue = 1
            
            // Slide to delete cell
            let remove = POPSpringAnimation(propertyNamed: kPOPLayerPositionX)
            remove?.toValue = -50
            remove?.dynamicsFriction = 100
            
            // Start the squeeze animation
            self.customizeSelectVw.pop_add(squeeze, forKey: "squeezeCell")
     
            // Fade in the active cell
            if (self.customizeSelectVw.pop_animation(forKey: "squeezeCell") != nil) {
                
                self.customizeActiveVw.pop_add(show, forKey: "activeCell")

            }
            
            // Delete the row
            if (self.customizeActiveVw.pop_animation(forKey: "activeCell") != nil){
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                    
                self.customizeSlideVw.pop_add(remove, forKey: "cell")
                    
                }
            }
            
        }
        
    }
    
    /**
     * Function to reset the customize animation
     *
     */
    
    func resetCustomizeAnimation (){
        
        // Fade the cell back
        let show = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
        show?.toValue = 0
        
        // Move the cell back
        let remove = POPSpringAnimation(propertyNamed: kPOPLayerPositionX)
        remove?.toValue = 60
        
        self.customizeActiveVw.pop_add(show, forKey: "activeCell")
        self.customizeSlideVw.pop_add(remove, forKey: "cell")
        
    }
    
    
    /*
     * Peak animations
     */
    
    func peakAnimation(){
        
        // 0.5 second delay to squeeze the cell in and out
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            
            // Corner radius
            let corner = POPSpringAnimation(propertyNamed: kPOPLayerCornerRadius)
            corner?.fromValue = 0
            corner?.toValue = 3
            self.peakDataVw.layer.pop_add(corner, forKey: "cornerCell")
            
            // Select cell animation
            let select = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            select?.fromValue = 1
            select?.toValue = 0
            
            // Show the data animation
            let show = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            show?.fromValue = 0
            show?.toValue = 1
            
            // Pull down cell animation
            let pull = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            pull?.toValue = CGRect(x: 0, y: 110, width: 118, height: 131)
            pull?.springBounciness = 1
            pull?.dynamicsFriction = 9
            
            self.peakSelectVw.pop_add(select, forKey: "fadeCell")
            
            self.peakSlideDownVw.pop_add(pull, forKey: "peakCell")
            
            if (self.peakSlideDownVw.pop_animation(forKey: "peakCell") != nil){
                self.peakDataVw.pop_add(show, forKey: "fadeInCell")
            }
            
        }
    }
    
    /*
     * Reset peak animations
     */
    
    func resetPeakAnimation(){
    
        // Reset Alphas
        let select = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
        select?.toValue = 1
        let show = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
        show?.toValue = 0
        
        // Reset the position
        let pull = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        pull?.toValue = CGRect(x: 0, y: 57, width: 118, height: 131)
        
        self.peakSelectVw.pop_add(select, forKey: "fadeCell")
        self.peakSlideDownVw.pop_add(pull, forKey: "peakCell")
        self.peakSelectVw.pop_add(show, forKey: "fadeInCell")
    }
    
    /*
     * Share animations
     */
    
    func shareAnimation() {
        
        // 0.5 second delay to squeeze the cell in and out
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            
            // Slide the cell to the left
            let slide = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            slide?.toValue = CGRect(x: -38, y: 25, width: 118, height: 35)
            slide?.dynamicsFriction = 15
            
            // Slide the cell to the right
            let back = POPSpringAnimation(propertyNamed: kPOPViewFrame)
            back?.toValue = CGRect(x: 0, y: 25, width: 118, height: 35)
            back?.dynamicsFriction = 15
            
            // Fade in overlay cell animation
            let select = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
            select?.fromValue = 0
            select?.toValue = 0.5
            select?.dynamicsFriction = 12
            
            // Drop the popover
            let drop = POPSpringAnimation(propertyNamed: kPOPViewFrame)
              drop?.fromValue = CGRect(x: 18, y: -84, width: 98, height: 94)
            drop?.toValue = CGRect(x: 18, y: 84, width: 98, height: 94)
            drop?.springBounciness = 1
            drop?.dynamicsFriction = 11
            
            self.shareSlideVw.pop_add(slide, forKey: "slideCell")
            
            if (self.shareSlideVw.pop_animation(forKey: "slideCell") != nil){
                self.shareOverlayVw.pop_add(select, forKey: "displayOverlay")
            }
            
            if (self.shareOverlayVw.pop_animation(forKey: "displayOverlay") != nil){
                
                // 0.5 second delay to squeeze the cell in and out
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                
                    self.shareSlideVw.pop_add(back, forKey: "slideBack")
                    self.sharePopOverVw.pop_add(drop, forKey: "dropPopOver")
                }
            }
        }
    }
    
    /*
     * Reset share animations
     */
    
    func resetShareAnimation() {
        
        // Fade back
        let select = POPSpringAnimation(propertyNamed: kPOPViewAlpha)
        select?.toValue = 0
        
        // Move back up
        let drop = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        drop?.toValue = CGRect(x: 18, y: -84, width: 98, height: 94)
        
        // Reset position
        self.shareOverlayVw.pop_add(select, forKey: "displayOverlay")
        self.sharePopOverVw.pop_add(drop, forKey: "dropPopOver")
        
    }

}

extension UIView {
    func fadeTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        self.layer.add(animation, forKey: kCATransitionFade)
    }
}
