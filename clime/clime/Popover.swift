//
//  Warning.swift
//  clime
//
//  Created by Tunde Adegoroye on 05/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class Popover {

    private var title: String!, info: String!
    private var image: UIImage?
    
    init(title: String, info: String, image: UIImage?) {
        
        self.title = title
        self.info = info
        self.image = image
    }
    
    // Function to display the error view controller
    public func showPopover(view: UIViewController){
    
        // Configure
        let popoverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popoverVC") as! PopoverViewController
        
        popoverVC.popoverTitle = self.title
        popoverVC.popoverInfo = self.info
        popoverVC.popoverImage = self.image
        
        popoverVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        popoverVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        

        view.present(popoverVC, animated: true, completion: nil)
        
    }
    
}
