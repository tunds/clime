//
//  Facebook.swift
//  clime
//
//  Created by Tunde Adegoroye on 20/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import PromiseKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class FacebookAuth {
    
    // Function to allow users to login to facebook
    public func login(withView: UIViewController) -> Promise<Void> {
    
        return Promise { success, fail in
            
            FBSDKLoginManager().logIn(withReadPermissions: ["public_profile"], from: withView, handler: { result, error in

                if let error = error {
                    
                    fail(error)
                    
                } else {
                    success()
                }
                
            })
            
        
        }
    }
    
    // Function to allow users to share the weather
    public func shareWeather(withView: UIViewController, withImage: UIImage) {
    
        let photo = FBSDKSharePhoto()
        photo.image = withImage
        photo.isUserGenerated = true
        
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        FBSDKShareDialog.show(from: withView, with: content, delegate: nil)
    }
    
    // Function to check if there's a facebook object on the device
    public func verfiy() -> Promise<Bool> {
    
        return Promise { success, fail in
        
            if FBSDKAccessToken.current() != nil {
                
                 success(true)
            }
            
            success(false)
            
        }
        
    }
    
    // Function to logout
    public func logout() -> Promise<Void> {
        
        return Promise { success, fail in
            
            FBSDKLoginManager().logOut()
            success()
            
        }
        
    }
    
}
