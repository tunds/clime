//
//  Misc.swift
//  clime
//
//  Created by Tunde Adegoroye on 28/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Spring

class Misc {

    /*
     * Function to pop a view into the view
     */
    public func popView(view: DesignableView) {
        
        view.animation = "pop"

        view.force = 1.4
        view.duration = 0.4
        view.delay = 0.3
        view.scaleX = 0
        view.scaleY = 0
        view.curve = "linear"
        
        view.animate()
        
    }
    
    /*
     * Function to pop a view into the view
     */
    public func popImage(view: DesignableImageView) {
        
        view.animation = "pop"
        
        view.duration = 0.9
        view.curve = "spring"

        view.animate()
        
    }


}
