//
//  ShareViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 17/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import TwitterKit
import PromiseKit
import Spring

class ShareViewController: UIViewController {

    public var icon = "", city = "", summary = ""
    public var temp = 0, time = 0
    
    internal let gradients = Gradients()
    internal let twitterAuth = TwitterAuth()
    internal let facebookAuth = FacebookAuth()
    
    @IBOutlet weak var shareContainerVw: DesignableView!
    @IBOutlet weak var weatherVw: UIView!
    @IBOutlet weak var weatherTimeLbl: UILabel!
    @IBOutlet weak var weatherImgVw: UIImageView!
    @IBOutlet weak var weatherTempLbl: UILabel!
    @IBOutlet weak var weatherCityLbl: UILabel!
    @IBOutlet weak var weatherSummaryLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = UnixConverter(unix: Int(Date().timeIntervalSince1970)).getDay().uppercased() == UnixConverter(unix: time).getDay().uppercased() ? "current" : "future"
        let apparent = Forecast(lat: 0, long: 0).getApperent(withTemp: temp)
        
        
        weatherTimeLbl.text = UnixConverter(unix: time).getDay(withComparison: Date())
        weatherVw.layer.insertSublayer(getGradientPeak(forWeather: icon, view: weatherVw.bounds), at: 0)
        weatherImgVw.image = UIImage(named: icon)
        weatherTempLbl.text = "\(temp)"
        weatherCityLbl.text = city
        weatherSummaryLbl.text = Message(icon: icon, temp: apparent, date: date).getMessage()
    }

    @IBAction func dismissDidTouch(_ sender: Any) {
        
        shareContainerVw.animation = "fall"
        shareContainerVw.animateToNext {
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func twitterDidTouch(_ sender: Any) {
        
        firstly {
        
            twitterAuth.login()
            
        }.then { _ -> Void in
            
        
            self.twitterAuth.shareWeather(withView: self,withImage: self.getGeneratedWeatherImage())
            
        }.catch { error -> Void in
            
            Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
        }
        
    }
    
    
    @IBAction func facebookDidTouch(_ sender: Any) {
        
        firstly {
            
           return Promise(value: facebookAuth.verfiy())
            
        }.then { result -> Void in
            
            if !result.value! {
            
                self.facebookAuth.login(withView: self)
            }
            
        }.then { _ -> Void in

           self.facebookAuth.shareWeather(withView: self, withImage: self.getGeneratedWeatherImage())

        }.catch { error -> Void in
            
            Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
        }
    
    }
    
    
    /**
     * Function to turn a view into a image
     * - return: The image
     */
    private func getGeneratedWeatherImage() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(weatherVw.bounds.size, weatherVw.isOpaque, 0)
        weatherVw.drawHierarchy(in: weatherVw.bounds, afterScreenUpdates: false)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    /**
     * Function to get the gradient based on the weather for the peak view
     * - parameter forWeather: the weather
     * - parameter view: the bounds of the peak view chart container
     * - return: The correct gradient
     *
     */
    private func getGradientPeak(forWeather: String, view: CGRect) -> CAGradientLayer{
        
        switch forWeather {
        case "clear-day":
            
            return gradients.clearDay(bounds: view)
            break;
        case "clear-night":
            return gradients.clearNight(bounds: view)
            break;
        case "partly-cloudy-night":
            return gradients.partlyCloudyNight(bounds: view)
            break;
        case "partly-cloudy-day":
            return gradients.partlyCloudyDay(bounds: view)
            break;
        case "cloudy":
            return gradients.cloudy(bounds: view)
            break;
        case "rain":
            return gradients.rain(bounds: view)
            break;
        case "sleet":
            return gradients.sleet(bounds: view)
            break;
        case "snow":
            return gradients.snow(bounds: view)
            break;
        case "wind":
            return gradients.wind(bounds: view)
            break;
        case "fog":
            return gradients.fog(bounds: view)
            break;
        default:
            break;
            
        }
        
        return gradients.defaultDay(bounds: view)
        
    }
}
