//
//  LocationTableViewCell.swift
//  clime
//
//  Created by Tunde Adegoroye on 16/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Spring
import RealmSwift
import MGSwipeTableCell

class LocationTableViewCell: MGSwipeTableCell {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var locationImgVw: DesignableImageView!
    
    
    /**
     * Function to configure the location cell
     * - parameter location: The location
     */
    public func configureLocationCell(location: Locations){
    
        locationLbl.text = location.name
        
        // If the location is active set the imageview and pop it
        if location.isActive {
        
            locationImgVw.isHidden = false
            Misc().popImage(view: locationImgVw)
            
        } else {
            locationImgVw.isHidden = true
        }
        
    }
    
}
