//
//  Message.swift
//  clime
//
//  Created by Tunde Adegoroye on 05/02/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import Foundation

class Message {
    
    
    private var icon = "", temp = "", date = ""

    private let messages = [
    
        "clear-day" : [
            
            "current": [
                "hot": [
                    "Get out the shades, it’s going to be a sunny day.",
                    "Gah lee it’s hot outside today right?",
                    "Do you like Frank Ocean? Go and enjoy his music in the sun.",
                    "Ayeeee you hear about the good news? The sun is out today!",
                    "Today might a good day to consider sun cream.",
                    "It’s time for you to go an slay in the sun."
                ],
                "cold": [
                    "Don’t be fooled by the sun, winter is coming.",
                    "Yeah the sun’s out today, but trust me it’s kinda cold.",
                    "It’s sunny but cold…I know life isn’t fair right."
                ],
                "mild": [
                    "Look outside the window you see that orange ball? That’s the sun."
                ]
            ],
            "future": [
                "hot": [
                    "Note to self get out the shades, it’s going to be a sunny.",
                    "Gah lee it’s going to be hot outside!",
                    "Do you like Frank Ocean? You can enjoy his music in the sun.",
                    "Ayeeee you hear about the good news? The sun is going to be out!",
                    "It might a good idea to consider sun cream.",
                    "It’s your time to go and slay in the sun."
                ],
                "cold": [
                    "Don’t be fooled by the sun, winter is coming.",
                    "Yeah the sun’s going to be out, but trust me it’s going to be cold.",
                    "It’s going to be sunny but cold…I know life isn’t fair right."
                ],
                "mild": [
                    "When you look outside the window that orange ball is the sun."
                ]
            ]
         
        ],
        "clear-night" : [
            "current": [
                "hot": [
                    "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
                    "Turn on all the fans it’s going to be a hot night tonight.",
                    "One fan isn’t going to be enough, you might melt."
                ],
                "cold": [
                    "Get some hot coco to warm you up tonight.",
                    "Your bed might feel like a slab of ice for the first 5 seconds, it’s that cold tonight.",
                    "Can you see cold air?, don’t worry you might survive the night…"
                ],
                "mild": [
                    "It’s going to be a clear night, wear your best outfit p.s. pj’s count too.",
                    "Get a nice view of the stars, it’s going to be a clear night tonight.",
                    "It’s a clear night cuddle up with someone, you cats or your pillow…",
                    "It’s going to be a clear night, btw you saw them stars?"
                ]
            ],
            "future": [
                "hot": [
                    "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
                    "Turn on all the fans it’s going to be a hot night.",
                    "One fan isn’t going to be enough, you might melt."
                ],
                "cold": [
                    "Plan the hot coco to warm you up tonight.",
                    "Your bed might feel like a slab of ice for the first 5 seconds.",
                    "If you see cold air?, don’t worry you might survive the night…"
                ],
                "mild": [
                    "It’s going to be a clear night, plan your best outfit p.s. pj’s count too.",
                    "Get a nice view of the stars, it’s going to be a clear night.",
                    "It’s going to be a clear night, btw you saw them stars?"
                ]
            ]

        ],
        "partly-cloudy-night" : [
            "current": [
                "hot": [
                    "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
                    "Turn on all the fans it’s going to be a hot night tonight.",
                    "One fan isn’t going to be enough, you might melt."
                ],
                "cold": [
                    "Get some hot coco to warm you up tonight.",
                    "Your bed might feel like a slab of ice for the first 5 seconds, it’s that cold tonight.",
                    "Can you see cold air?, don’t worry you might survive the night…"
                ],
                "mild": [
                    "It’s going to be a cloudy night, wear your best outfit p.s. pj’s count too.",
                    "Get a nice view of the stars, it’s going to be a cloudy night tonight.",
                    "It’s a clear night cuddle up with someone, you cats or your pillow…",
                    "It’s going to be a cloudy night, btw you saw them stars?"
                ]
            ],
            "future": [
                "hot": [
                    "It’s going to be a warm night, one leg in one leg out sounds like a good plan.",
                    "Turn on all the fans it’s going to be a hot night.",
                    "One fan isn’t going to be enough, you might melt."
                ],
                "cold": [
                    "Plan the hot coco to warm you up tonight.",
                    "Your bed might feel like a slab of ice for the first 5 seconds.",
                    "If you see cold air?, don’t worry you might survive the night…"
                ],
                "mild": [
                    "It’s going to be a cloudy night, plan your best outfit p.s. pj’s count too.",
                    "Get a nice view of the stars, it’s going to be a cloudy night.",
                    "It’s going to be a cloudy night, btw you saw them stars?"
                ]
            ]
        ],
        "partly-cloudy-day" : [
            "current": [
                "hot": [
                    "Although the clouds are out it’s still going to be a hot day."
                ],
                "cold": [
                    "Don’t let the sun fool you it’s still going to be cold."
                ],
                "mild": [
                    "The sun and clouds are both fighting for your attention."
                ]
            ],
            "future": [
                "hot": [
                   "Although the clouds are out it’s still going to be a hot day."
                ],
                "cold": [
                    "Don’t let the sun fool you it’s still going to be cold."

                ],
                "mild": [
                    "The sun and clouds are both going to be fighting for your attention."
                ]
            ]
        ],
        "cloudy" : [
            "current": [
                "hot": [
                    "Although the clouds are out it’s still going to be a hot day.",
                    "It’s going to look grey today because it’s cloudy…"
                ],
                "cold": [
                    "So it’s cloudy and cold… great."
                ],
                "mild": [
                    "The sun’s on its break, today you’ll have to deal with the clouds.",
                    "It’s miserable but remember you’re a star. Cheesy but what you gonna do?",
                    "Oh wait the clouds are out today…"
                ]
            ],
            "future": [
                "hot": [
                    "Although the clouds are out it’s still going to be a hot day.",
                    "It’s going to look grey today because it’s cloudy…"
                ],
                "cold": [
                    "So it’s going to be cloudy and cold… great."
                
                ],
                "mild": [
                    "The sun’s on its break, you’ll have to deal with the clouds.",
                    "Oh wait the clouds are going to be out…"
                ]
            ]
        ],
        "rain" : [
            "current": [
                "hot": [
                    "So it’s hot and raining… I wonder if the water burns?",
                    "Its hot and raining I just don’t even know anymore."
                ],
                "cold": [
                    "Today would be a valid day to stay in all day and play video games.",
                    "It’s cold and raining… (Hey Arthur fist)."
                ],
                "mild": [
                    "Listening to Drake in this rain is only going to make this day sadder",
                    "It’s raining sooo, do you want to just binge on YouTube videos all day?",
                    "It’s raining… Who needs the outdoors anyways"
                ]
            ],
            "future": [
                "hot": [
                    "Its going to be hot and raining I just don’t even know anymore."
                ],
                "cold": [
                    "It would be a valid excuse to stay in all day and play video games.",
                    "It’s going to be cold and raining… (Hey Arthur fist)."

                ],
                "mild": [
                    "Listening to Drake in this rain is only going to make this day sadder.",
                    "It’s going to rain… Who needs the outdoors anyways"
                ]
            ]
            
        ],
        "sleet" : [
            "current": [
                "hot": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ],
                "cold": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ],
                "mild": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ]
            ],
            "future": [
                "hot": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ],
                "cold": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ],
                "mild": [
                    "Watch out for that sleet, if one gets in your eye it could be game over.",
                    "That’s not snow it’s it evil sibling sleet."
                ]
            ]
        ],
        "snow" : [
            "current": [
                "hot": [
                    "How are your snowball sniping skills?",
                    "IT’S SNOWINGGGGGGG!",
                    "Go on outside and throw a couple of snowballs.",
                    "We’ve been blessed with snow today.",
                    "You see that jumper you’ve been avoiding for months… It’s time."
                ],
                "cold": [
                    "How are your snowball sniping skills?",
                    "IT’S SNOWINGGGGGGG!",
                    "Go on outside and throw a couple of snowballs.",
                    "We’ve been blessed with snow today.",
                    "You see that jumper you’ve been avoiding for months… It’s time."
                ],
                "mild": [
                    "How are your snowball sniping skills?",
                    "IT’S SNOWINGGGGGGG!",
                    "Go on outside and throw a couple of snowballs.",
                    "We’ve been blessed with snow today.",
                    "You see that jumper you’ve been avoiding for months… It’s time."
                ]
            ],
            "future": [
                "hot": [
                    "How are your snowball sniping skills?",
                    "IT’S GOING TO BE SNOWINGGGGGGG!",
                    "You see that jumper you’ve been avoiding for months… It’s time."
                ],
                "cold": [
                    "How are your snowball sniping skills?",
                    "IT’S GOING TO BE SNOWINGGGGGGG!",
                    "You see that jumper you’ve been avoiding for months… It’s time."                ],
                "mild": [
                    "How are your snowball sniping skills?",
                    "IT’S GOING TO BE SNOWINGGGGGGG!",
                    "You see that jumper you’ve been avoiding for months… It’s time."
                ]
            ]
        ],
        "wind" : [
            "current": [
                "hot": [
                    "If you’re not already flying in the air, did you know it’s going to be windy?",
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ],
                "cold": [
                    "If you’re not already flying in the air, did you know it’s going to be windy?",
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ],
                "mild": [
                    "If you’re not already flying in the air, did you know it’s going to be windy?",
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ]
            ],
            "future": [
                "hot": [
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ],
                "cold": [
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ],
                "mild": [
                    "Remember that time you’re parents said you should wear a jacket, it might be a good idea to listen to them.",
                    "It’s might be chilly because of the wind."
                ]
            ]
        ],
        "fog" : [
            "current": [
                "hot": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ],
                "cold": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ],
                "mild": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ]
            ],
            "future": [
                "hot": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ],
                "cold": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ],
                "mild": [
                    "It’s going to be foggy, please don’t run over that cat…",
                    "It may look like a ghost town due to the fog."
                ]
            ]

        ]
    ]
    
    init(icon: String, temp: String, date: String) {
        
        self.icon = icon
        self.temp = temp
        self.date = date
        
    }
    
    // Get the message
    func getMessage() -> String {
    
        return generateMessage()
    }
    
    // Get a random message from the array object
    private func generateMessage() -> String {
    
        let index = Int(arc4random_uniform(UInt32((messages[icon]![date]![temp]?.count)!)))
        
        return messages[icon]![date]![temp]![index]
    }
}
