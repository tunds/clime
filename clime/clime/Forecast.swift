//
//  Forecast.swift
//  clime
//
//  Created by Tunde Adegoroye on 28/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import Alamofire
import SwiftyJSON
import PromiseKit

class Forecast {

    private var lat = 0.0, long = 0.0
    private var units = "auto"
    private let secretKey = "a4ff17cd9b8902309106b8801cf48a66/", api = "https://api.darksky.net/forecast/"
    private let config = Config()
    
    init(lat: Double, long: Double) {
        
        self.lat = lat
        self.long = long
    }
    

    /*
     * Function to get the current forecast
     * - parameter date: The date for the weather forecast
     * - return: JSON object of the weather
     */
    private func currentForecastCall(date: Int) -> Promise<JSON> {
        
        units = config.verfiyTempType() ? "si" : "us"
        
        return Promise { success, fail in
   
                // Example: https://api.darksky.net/forecast/a4ff17cd9b8902309106b8801cf48a66/37.8267,-122.4233?exclude=currently,minutely,hourly
                Alamofire.request(api + secretKey + "\(lat),\(long),\(date)?exclude=daily,minutely,hourly,alerts,flags&units=\(units)").responseJSON { response in
                    
                    switch response.result {
                    case .success(let value):

                       let val = JSON(value)
                       success(val["currently"])
                     
                    case .failure(let error):
                        
                        fail(error)
                        
                    }
            }
            
        }
    }

    /*
     * Function to get the forecast
     * - parameter date: The dates for the weather forecast
     * - return: JSON object of the weather
     */
    
    public func getForecast(dates: [Int]) -> Promise<[JSON]> {
    
        return Promise { success, fail in
            
            var forecast = [JSON]()
            
            currentForecastCall(date: dates[0]).then { result -> Promise<JSON> in
            
                forecast.append(result)
                return self.currentForecastCall(date: dates[1])
                
            }.then { result -> Promise<JSON> in
                
                forecast.append(result)
                return self.currentForecastCall(date: dates[2])
                
            }.then { result -> Promise<JSON> in
                    
                    forecast.append(result)
                    return self.currentForecastCall(date: dates[3])
                    
            }.then { result -> Promise<JSON> in
                    
                    forecast.append(result)
                    return self.currentForecastCall(date: dates[4])
                    
            }.then { result -> Promise<JSON> in
                    
                    forecast.append(result)
                    return self.currentForecastCall(date: dates[5])
                    
            }.then { result -> Void in
                
                forecast.append(result)
                
            }.always {
                
                success(forecast)
            }

        }
    }
    
    
    /*
     * Function to get hourly forecast for a specific time
     * - parameter time: The time for the weather forecast
     * - return: JSON object of the weather
     */
    public func getHourlyForecast(time: Int) -> Promise<JSON> {
        
        units = config.verfiyTempType() ? "si" : "us"
        
        return Promise { success, fail in
            
            // Example: https://api.darksky.net/forecast/a4ff17cd9b8902309106b8801cf48a66/42.3601,-71.0589,409467600?exclude=currently,flags,alerts,daily&units=si
            Alamofire.request(api + secretKey + "\(lat),\(long),\(time)?exclude=currently,flags,alerts,daily&units=\(units)").responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
   
                    success(json["hourly"]["data"])
                    
                case .failure(let error):
                    
                    fail(error)
                    
                }
            }
        }
    }
    
    /*
     * Function to get the apparent weather 
     * - parameter withTemp: The temp for the weather forecast
     * - return: The type of weather
     */
    public func getApperent(withTemp: Int) -> String {
        
        switch config.verfiyTempType() {
        case true:
            
            // Celcius checks if it's hot, cold and then finally mild
            if withTemp >= 25 {
                return "hot"
            } else if withTemp <= 4 {
                return "cold"
            } else {
                return "mild"
            }
            
        case false:
            
            // Farenheit checks if it's hot, cold and then finally mild
            if withTemp >= 80 {
                return "hot"
            } else if withTemp <= 40 {
                return "cold"
            } else {
                return "mild"
            }

        }
    }
}
