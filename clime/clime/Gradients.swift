//
//  Gradients.swift
//  clime
//
//  Created by Tunde Adegoroye on 19/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit

class Gradients {

    private let gradientLyr = CAGradientLayer()
    
    public func clearDay(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:1.00, green:0.86, blue:0.35, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.97, green:0.72, blue:0.35, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func clearNight(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.53, green:0.43, blue:1.00, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.40, green:0.29, blue:1.00, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func rain(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.13, green:0.76, blue:0.99, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.11, green:0.61, blue:1.00, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func partlyCloudyDay(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.31, green:0.63, blue:1.00, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.13, green:0.53, blue:0.96, alpha:1.00).cgColor as CGColor
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func partlyCloudyNight(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.43, green:0.32, blue:1.00, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.41, green:0.20, blue:1.00, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func cloudy(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.28, green:0.57, blue:0.90, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.10, green:0.42, blue:0.76, alpha:1.00).cgColor as CGColor
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func sleet(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.73, green:0.78, blue:0.88, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.42, green:0.52, blue:0.71, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
    public func snow(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.80, green:0.89, blue:1.00, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.67, green:0.79, blue:1.00, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
        
        return gradientLyr
    }
    
    public func wind(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.65, green:0.75, blue:0.91, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.38, green:0.56, blue:0.91, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]

        return gradientLyr
    }
    
    public func fog(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.65, green:0.75, blue:0.91, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.38, green:0.57, blue:0.91, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
       
    
        return gradientLyr
    }
    
    public func defaultDay(bounds: CGRect) -> CAGradientLayer {
        
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.31, green:0.63, blue:1.00, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.13, green:0.53, blue:0.96, alpha:1.00).cgColor as CGColor
        gradientLyr.colors = [start, end]
       
        
        return gradientLyr
    }
    
}


