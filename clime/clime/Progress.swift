//
//  Progress.swift
//  clime
//
//  Created by Tunde Adegoroye on 06/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import SVProgressHUD

class Progress {
    
    private let primaryColor = UIColor(red:0.16, green:0.54, blue:0.94, alpha:1.00)
    private let supportingColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.00)
    
    init() {
        
        // Configure progress
        SVProgressHUD.setRingThickness(6)
        SVProgressHUD.setBackgroundColor(supportingColor)
        SVProgressHUD.setBackgroundLayerColor(supportingColor)
        SVProgressHUD.setForegroundColor(primaryColor)
    }
    
    /**
     * Function to show the progress bar
     */
    public func showProgress(withText: String) {
        
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.show(withStatus: withText)
    }
    
    /**
     * Function to hide the progress bar
     */
    public func dismissProgress(){
        
        SVProgressHUD.dismiss()
    }
}

