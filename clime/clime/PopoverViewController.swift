//
//  PopoverViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 05/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Spring

class PopoverViewController: UIViewController {
    
    // Popover config fields
    public var popoverImage: UIImage?
    public var popoverTitle = "", popoverInfo = ""

    @IBOutlet weak var popoverContainerVw: DesignableView!
    @IBOutlet weak var popoverImgVw: DesignableImageView!
    @IBOutlet weak var popoverTitleLbl: UILabel!
    @IBOutlet weak var popoverInfoLbl: UILabel!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Assign the image and the text
        popoverImgVw.image = popoverImage
        popoverTitleLbl.text = popoverTitle

        // Configure line spacing
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.5
        let attrString = NSMutableAttributedString(string: popoverInfo)
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        
        popoverInfoLbl.attributedText = attrString
        popoverInfoLbl.textAlignment = NSTextAlignment.center
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissDidTouch(_ sender: Any) {
        
        popoverContainerVw.animation = "fall"
        popoverContainerVw.animateToNext {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func closeDidTouch(_ sender: Any) {
        
        popoverContainerVw.animation = "fall"
        popoverContainerVw.animateToNext {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}
