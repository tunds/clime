//
//  Chart.swift
//  clime
//
//  Created by Tunde Adegoroye on 08/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import Charts

class Chart {

    // The chart
    private var lineChart: LineChartView!

    // The data to pass through
    private var dataPoints: [String]!
    private var values: [Double]!
    private var icons: [Double: String]!
    
    // Data collection for entries
    private var data : [ChartDataEntry] = []
    
    // The x axis
    private let xaxis = XAxis()

    // The formatter for the axis
    private var axisFormatter : AxisFormatter!
    
    init(lineChart: LineChartView, dataPoints: [String], values: [Double], icons: [Double: String]) {
        
        self.lineChart = lineChart
        self.dataPoints = dataPoints
        self.values = values
        self.icons = icons
    }
    
    /**
     * Function to setup the chart
     */
    public func setupChart(){
    
        // Set up x axis
        axisFormatter = AxisFormatter(myArr: dataPoints)
        
        // Add the data
        for i in 0..<dataPoints.count {
            
            data.append(ChartDataEntry(x: Double(i) , y: values[i]))
            
        }
        
        // Set up the x axis
        xaxis.valueFormatter = axisFormatter
        lineChart.xAxis.valueFormatter = xaxis.valueFormatter
        
        // Configure the dataset
        let lineChartDataSet = LineChartDataSet(values: data, label: "Weather throughout the day")
        lineChartDataSet.notifyDataSetChanged()
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        
        /**
         * Styling the graph
         */
        
        // Hide gridlines
        lineChart.drawGridBackgroundEnabled = false
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.rightAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        
        // Hide Axis lines
        lineChart.leftAxis.drawAxisLineEnabled = false
        lineChart.xAxis.drawAxisLineEnabled = false
        
        // Hide legend & info
        lineChart.legend.enabled = false
        lineChart.chartDescription?.enabled = false
        
        // Hide right axis and move label to the bottom
        lineChart.xAxis.labelPosition = .bottom
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.enabled = false
        
        // Hide horizontal lines
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
        
        // Remove duplication
        lineChart.xAxis.granularityEnabled = true
        lineChart.xAxis.granularity = 1.0
        lineChart.xAxis.labelCount = 24
        
        // General styles
        lineChart.backgroundColor = UIColor(red:1, green:1, blue:1, alpha:0)
        lineChart.minOffset = 25
        lineChart.isOpaque = false
        lineChartDataSet.valueTextColor = UIColor(red:0.16, green:0.21, blue:0.23, alpha:1.00)
        lineChartDataSet.valueFont = UIFont(name: "fontello", size: 15)!
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.circleRadius = 5
        lineChartDataSet.circleHoleRadius = 0
        lineChartDataSet.lineWidth = 4
        lineChartDataSet.setCircleColor(UIColor(red:0.97, green:0.97, blue:0.99, alpha:1.00))
        lineChartDataSet.setColor(UIColor(red:0.97, green:0.97, blue:0.99, alpha:1.00))
        
        /**
         * Configuring interactions
         */
        
        lineChart.setScaleEnabled(true)
        lineChart.doubleTapToZoomEnabled = false
        lineChart.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
        
        // Animate
        lineChart.animate(yAxisDuration: 1.5, easingOption: .easeInOutCubic)
        
        // Format the chart
        lineChart.xAxis.labelTextColor = UIColor(red:0.96, green:0.97, blue:0.97, alpha:1.00)
        lineChart.xAxis.labelFont = UIFont(name: "Lato-Black", size: 7.5)!
        lineChart.xAxis.granularity = 1
        lineChart.xAxis.granularityEnabled = true
        lineChartData.setValueFormatter(ValueFormatter(icons: icons))
        
        // Assign data
        lineChart.data = lineChartData
        
    }
    
}




@objc(AxisFormatter)
class AxisFormatter: NSObject, IAxisValueFormatter {
    
    // The array of values to show on x axis
    private var myArr: [String]!
    
    init(myArr: [String]) {
        
        self.myArr = myArr
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let val = Int(value)
        
        // Make sure the value is within range and then return the value
        if val >= 0 && val < myArr.count {
            
            return "\(myArr[Int(val)])"
            
        }
        
        return ""
    }
    
}

class ValueFormatter: NSObject, IValueFormatter {
    
    
    private var icons: [Double: String]?
    
    init(icons: [Double: String]) {
        self.icons = icons
    }
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        
        let icon = getWeatherFont(withIcon: (icons?[value])!)
        return "\(Int(value))\u{00B0}  \(icon)"
    }
    
    
    private func getWeatherFont(withIcon: String) -> String {
    
        switch withIcon {
        case "clear-day":
            return "\u{0108}"
        case "clear-night":
            return "\u{0109}"
        case "partly-cloudy-night":
            return "\u{010D}"
        case "partly-cloudy-day":
            return "\u{010C}"
        case "cloudy":
            return "\u{010C}"
        case "rain":
            return "\u{010E}"
        case "sleet":
            return "\u{010F}"
        case "snow":
            return "\u{0110}"
        case "wind":
            return "\u{0111}"
        case "fog":
            return "\u{010B}"
        default:
          return "\u{010A}"
        }
        
        return ""
    }
}

