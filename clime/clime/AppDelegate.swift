//
//  AppDelegate.swift
//  clime
//
//  Created by Tunde Adegoroye on 13/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import CoreData
import GooglePlaces
import Fabric
import TwitterKit
import FBSDKCoreKit
import UserNotifications
import Parse
import PromiseKit
import iRate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let config = Config()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Customise the rating text
        iRate.sharedInstance().messageTitle = "Show us some ❤️"
        iRate.sharedInstance().message = "Are you enjoying the weather we’re giving you? We’d appreciate your support if you took a moment to rate us ☀️"
        
        // Enable preview mode
        // iRate.sharedInstance().previewMode = true
        
        // Register Parse configuration
        let configuration = ParseClientConfiguration {
            $0.applicationId = "OdEnnQICwhD5G9HlTshL87jPmABy6qDpcBPV1Tgu"
            $0.clientKey = "74tJ2NhVZq01aTacUEC2yPHiLGrc4dxOXQsKDQwW"
            $0.server = "https://parseapi.back4app.com"
            $0.isLocalDatastoreEnabled = true
        }
        
        Parse.initialize(with: configuration)
        
        // Register for notifications for both iOS 10 and below
        if #available(iOS 10.0, *) {
            
            let notifCenter = UNUserNotificationCenter.current()
            notifCenter.delegate = self
            notifCenter.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { result, error in
                
                if error == nil {
                    
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
            
        } else {
            
            // Only for iOS 9 devices
            let notificationTypes: UIUserNotificationType = [.alert, .badge, .sound]
            let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        // Universal navigation bar styles
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBarAppearace.shadowImage = UIImage()
        
        // Universal tab bar styles
        let tabBarApperance = UITabBar.appearance()
        tabBarApperance.backgroundImage = UIImage()
        tabBarApperance.shadowImage = UIImage()
        
        // Intialize twitter and facebook
        Fabric.with([Twitter.self])
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Override point for customization after application launch.
        GMSPlacesClient.provideAPIKey("AIzaSyDThQHzkzWTuxoTmXaWWgFaLh2Mb4FwXvU")
        
        if config.verfiySeenOnboarding() {
            
            // Go to the tab view
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "weatherVC") as UIViewController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = initialViewControlleripad
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        if Twitter.sharedInstance().application(app, open:url as URL, options: options) ||
            FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation]) {
            
            return true
        }
        
        return false
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(deviceToken)
        
        // After saving the token for push notifications save the custom fields
        installation?.saveInBackground(block: { result, error in
            
            if result {
                
                let location = Location()
                
                // Get the timezone
                if let identifier = installation?.timeZone {
                    
                    // Get the active location if there is one and saved it to the push notification object
                    if let currentLocation = location.getActiveLocation().first {
                        
                        // Save the push notification object
                        firstly {
                            
                            location.getCityName(lat: currentLocation.latitude, long: currentLocation.longitude)
                            
                            }.then { city -> Void in
                                
                                self.savePushObject(withActiveState: true, withIdentifier: identifier, withCity: city, long: currentLocation.longitude, lat: currentLocation.latitude)
                                
                        }
                        
                    } else {
                        
                        
                        // Find the users location and saved it to push notification object
                        firstly {
                            
                            location.getCurrentPlace()
                            
                            }.then { place in
                                
                                return when(fulfilled: location.getCityName(lat: place.coordinate.latitude,
                                                                            long: place.coordinate.longitude),
                                            self.getLong(location: place), self.getLat(location: place))
                                
                                
                            }.then { city, lat, long -> Void in
                                
                                self.savePushObject(withActiveState: true, withIdentifier: identifier, withCity: city, long: long, lat: lat)
                                
                        }
                        
                    }
                }
                
            }
        })
        
        PFPush.subscribeToChannel(inBackground: "globalChannel")
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PFPush.handle(userInfo)
        
        
    }
    
    // MARK: Remote notifications iOS 10 setup
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
        
    }
    
    // MARK: Class functions
    
    private func savePushObject(withActiveState: Bool, withIdentifier: String, withCity: String, long: Double, lat: Double) {
        
        // Enable a anon user to save for the push notifications
        PFUser.enableAutomaticUser()
        let user = PFUser.current()
        let installation =  PFInstallation.current()
        installation?.setObject(user, forKey: "user")
        installation?.setObject(withActiveState, forKey: "notificationsActive")
        installation?.setObject(withIdentifier, forKey: "timeZoneIdentifier")
        installation?.setObject(withCity, forKey: "timeZoneCity")
        installation?.setObject(lat, forKey: "latitude")
        installation?.setObject(long, forKey: "longitude")
        installation?.saveInBackground(block: { result, error in
            
            if result {
                
                do {
                    try installation?.fetch()
                } catch {}
            }
        })
        
    }
    
    /**
     * Function to get the latitude
     * - parameter location: the location from the google api
     * - return: The latitude
     */
    private func getLat(location: GMSPlace) -> Promise<Double> {
        
        return Promise { val, fail in
            
            val(location.coordinate.latitude)
        }
        
    }
    
    /**
     * Function to get the longitude
     * - parameter location: the location from the google api
     * - return: The longitude
     */
    private func getLong(location: GMSPlace) -> Promise<Double> {
        
        return Promise { val, fail in
            val(location.coordinate.longitude)
        }
    }
    
    
}
