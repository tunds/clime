//
//  WeatherReadingTableViewCell.swift
//  clime
//
//  Created by Tunde Adegoroye on 25/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftyJSON

class WeatherReadingTableViewCell: UITableViewCell {

    @IBOutlet weak var weatherVw: UIView!
    @IBOutlet weak var weatherImgVw: UIImageView!
    @IBOutlet weak var weatherTempLbl: UILabel!
    @IBOutlet weak var weatherTimeLbl: UILabel!
    @IBOutlet weak var weatherSummaryLbl: UILabel!
    
    internal let gradients = Gradients()
    
    /**
     * Function to configure data for the weather cell
     * - parameter readings: The 24hr weather forecast throughout the day
     * - parameter height: The height of the cell
     *
     */
    public func configureWeather(reading: JSON, height: CGFloat){

        // Make sure the values exists before manipulating data
        if let image = reading["icon"].string,
            let temp = reading["temperature"].int,
            let time = reading["time"].int {
 
            let date = UnixConverter(unix: Int(Date().timeIntervalSince1970)).getDay().uppercased() == UnixConverter(unix: time).getDay().uppercased() ? "current" : "future"
            let apparent = Forecast(lat: 0, long: 0).getApperent(withTemp: temp)
            
            
            // Add the gradient to the cell Message(
            weatherVw.layer.insertSublayer(getGradient(forWeather: image, height: height), at: 0)
            
            // Assign UI elements
            weatherImgVw.image = UIImage(named: image)
            weatherTempLbl.text = "\(temp)"
            weatherTimeLbl.text = UnixConverter(unix: time).getTime()
            weatherSummaryLbl.text = Message(icon: image, temp: apparent, date: date).getMessage()
        }
        
    }
    
    /**
     * Function to get the gradient based on the weather
     * - parameter forWeather: the weather
     * - parameter height: the height of the weather cell
     * - return: The correct gradient
     *
     */
    private func getGradient(forWeather: String, height: CGFloat) -> CAGradientLayer{
        
        switch forWeather {
        case "clear-day":
            
            return gradients.clearDay(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "clear-night":
            return gradients.clearNight(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "partly-cloudy-night":
            return gradients.partlyCloudyNight(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "partly-cloudy-day":
            return gradients.partlyCloudyDay(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "cloudy":
            return gradients.cloudy(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "rain":
            return gradients.rain(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "sleet":
            return gradients.sleet(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "snow":
            return gradients.snow(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "wind":
            return gradients.wind(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        case "fog":
            return gradients.fog(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
            break;
        default:
            break;
            
        }
        
        return gradients.defaultDay(bounds: CGRect(x: 0, y: 0, width: weatherVw.bounds.width, height: height))
        
    }
    
}
