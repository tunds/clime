//
//  ShakeViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 08/02/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Spring

class ShakeViewController: UIViewController {

    let config = Config()
    
    @IBOutlet weak var shakeImgVw: DesignableImageView!
    @IBOutlet weak var shakeContainerVw: DesignableView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        /**
         * Animate the container to fade in down first
         * then fade in the phone
         * then rotate the phone forever
         */
        shakeContainerVw.animation = "fadeInDown"
        shakeContainerVw.delay = 0.2
        shakeContainerVw.duration = 0.7
        shakeContainerVw.animate()
        shakeContainerVw.animateNext {
            
            self.shakeImgVw.animation = "fadeIn"
            self.shakeImgVw.duration = 0.3
            self.shakeImgVw.animate()
            
            self.shakeImgVw.animateNext {
                
                self.shakeImgVw.animation = "swing"
                self.shakeImgVw.repeatCount = 99999
                self.shakeImgVw.duration = 1.1
                self.shakeImgVw.animate()
                
            }
            
        }
        
        
    }
    
    @IBAction func closeDidTouch(_ sender: Any) {
        
        /**
         * Hide the image
         * Once the container has fallen then dismiss the view
         */
        self.shakeImgVw.isHidden = true
        shakeContainerVw.animation = "fall"
        shakeContainerVw.animateNext {
            self.config.setShakeableSeen()
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    @IBAction func dismissDidTouch(_ sender: Any) {
        
        self.shakeImgVw.isHidden = true
        shakeContainerVw.animation = "fall"
        shakeContainerVw.animateNext {
            self.config.setShakeableSeen()
            self.dismiss(animated: true, completion: nil)
        }
        
    }

}
