//
//  WeatherTableViewCell.swift
//  clime
//
//  Created by Tunde Adegoroye on 06/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftyJSON
import Charts
import Spring
import MGSwipeTableCell
import PromiseKit

class WeatherTableViewCell: MGSwipeTableCell {

    // Expandable tableview: https://www.youtube.com/watch?v=LqHzHkuXRxQ
    
    internal var weatherTimes = [String]()
    internal var weatherTemps = [Double]()
    internal var weatherIcons = [Double: String]()
    
    internal let gradients = Gradients()
    
    @IBOutlet weak var weatherVw: UIView!
    @IBOutlet weak var peakVw: UIView!
    @IBOutlet weak var peakVwLineChart: LineChartView!

    @IBOutlet weak var weatherGradientVw: UIView!
    @IBOutlet weak var weatherImgVw: UIImageView!
    @IBOutlet weak var weatherTempLbl: UILabel!
    @IBOutlet weak var weatherDayLbl: UILabel!
    
    @IBOutlet weak var peakDayLbl: UILabel!
    @IBOutlet weak var peakDescLbl: UILabel!
    @IBOutlet weak var peakWindSpeedLbl: UILabel!
    @IBOutlet weak var peakVisibilityLbl: UILabel!
    @IBOutlet weak var peakHumdityLbl: UILabel!
    @IBOutlet weak var peakTempLbl: UILabel!
    @IBOutlet weak var peakGradientVw: DesignableView!
    
    /**
     * Function to get the weather for a day
     * - parameter readings: The 24hr weather forecast throughout the day
     * - parameter weather: The weather forecast
     * - return: Void
     */
    public func configurePeakWeather(readings: JSON, weather: JSON) -> Promise<Void> {
        
        // Make sure the values exists before manipulating data
        if let type = weather["icon"].string,
            let temp = weather["apparentTemperature"].int,
            let day = weather["time"].int {
            
            // Get the configuration for the random Message
            let date = UnixConverter(unix: Int(Date().timeIntervalSince1970)).getDay().uppercased() == UnixConverter(unix: day).getDay().uppercased() ? "current" : "future"
            let apparent = Forecast(lat: 0, long: 0).getApperent(withTemp: temp)
            
            
            // Add the gradient to the cell
            peakGradientVw.layer.insertSublayer(getGradientPeak(forWeather: type, view: peakGradientVw.bounds), at: 0)
            

            let day = UnixConverter(unix: Int(Date().timeIntervalSince1970)).getDay().uppercased() == UnixConverter(unix: day).getDay().uppercased() ? "CURRENTLY" : UnixConverter(unix: day).getDay().uppercased()
            
            peakDayLbl.text = day
            peakDescLbl.text = Message(icon: type, temp: apparent, date: date).getMessage()
            peakTempLbl.text = "\(temp)"
            
            if let wind = weather["windSpeed"].double,
                let humidity = weather["humidity"].double,
                let visibility = weather["visibility"].int {
                
                peakWindSpeedLbl.text = "\(wind) mph"
                peakHumdityLbl.text = "\(humidity)%"
                peakVisibilityLbl.text = "\(visibility)mi"
                
            } else {
                
                peakWindSpeedLbl.text = "N/A"
                peakHumdityLbl.text = "N/A"
                peakVisibilityLbl.text = "N/A"

            }
            
            
            
        }
        
        return Promise { success, fail in
            
         let requestGroup = DispatchGroup()
            
            // Empty all the arrays
            weatherTimes.removeAll()
            weatherTemps.removeAll()
            var limit = 6, position = 0
            
            // If the array isn't empty
            if let _ = readings.arrayObject {
                
                for (_, val):(String, JSON) in readings {
                     requestGroup.enter()
                    
                    if let time = val["time"].int,
                       let temp = val["apparentTemperature"].int,
                       let icon = val["icon"].string {
                        
                        // Get the time one hour from now
                        if UnixConverter(unix: Int(Date().timeIntervalSince1970)).get24HourTime() <= UnixConverter(unix: time).get24HourTime() {
                            
                            // Limit the graph to 8 hours
                            if position < limit {
                                
                                position += 1
                                weatherTimes.append(UnixConverter(unix: time).getTime())
                                weatherTemps.append(Double(temp))
                                weatherIcons[Double(temp)] = icon
                                requestGroup.leave()
                                
                            }
                        }
                    }
                }
            }
            
            requestGroup.notify(queue: DispatchQueue.main, execute: {
               
                success()
            })
            
            
        }
        
        
    }
    
    
    /**
     * Function to get the weather for a day
     * - parameter readings: The weather forecast
     * - parameter height: The height of the cell
     * - return: Void
     */
    public func configureWeather(reading: JSON, height: CGFloat){
        
        // Make sure the values exists before manipulating data
        if let image = reading["icon"].string,
            let temp = reading["apparentTemperature"].int,
            let day = reading["time"].int {
            
            let day = UnixConverter(unix: Int(Date().timeIntervalSince1970)).getDay().uppercased() == UnixConverter(unix: day).getDay().uppercased() ? "CURRENTLY" : UnixConverter(unix: day).getDay().uppercased()
            
            // Add the gradient to the cell
            weatherGradientVw.layer.insertSublayer(getGradient(forWeather: image, height: height), at: 0)
            
            // Show the weather view cell
            weatherVw.isHidden = false
            
            // Assign UI elements
            weatherImgVw.image = UIImage(named: image)
            weatherTempLbl.text = "\(temp)"
            weatherDayLbl.text = day
        }
        
    }
    
    
    /**
     * Function to hide the weather cell
     */
    public func hideWeather() -> Promise<Void> {
        
        return Promise { success, fail in
        
            // Hide the weather view cell so the peak is fully displayed
            weatherVw.isHidden = true
            success()
            
        }
        
    }
    
    /**
     * Function to configure the chart
     *
     */
    public func configChart(){
    
        Chart(lineChart: peakVwLineChart, dataPoints: weatherTimes, values: weatherTemps, icons: weatherIcons).setupChart()
    }
    
    /**
     * Function to get the gradient based on the weather for the peak view
     * - parameter forWeather: the weather
     * - parameter view: the bounds of the peak view chart container
     * - return: The correct gradient
     *
     */
    private func getGradientPeak(forWeather: String, view: CGRect) -> CAGradientLayer{
        
        switch forWeather {
        case "clear-day":
            
            return gradients.clearDay(bounds: view)
            break;
        case "clear-night":
            return gradients.clearNight(bounds: view)
            break;
        case "partly-cloudy-night":
            return gradients.partlyCloudyNight(bounds: view)
            break;
        case "partly-cloudy-day":
            return gradients.partlyCloudyDay(bounds: view)
            break;
        case "cloudy":
            return gradients.cloudy(bounds: view)
            break;
        case "rain":
            return gradients.rain(bounds: view)
            break;
        case "sleet":
            return gradients.sleet(bounds: view)
            break;
        case "snow":
            return gradients.snow(bounds: view)
            break;
        case "wind":
            return gradients.wind(bounds: view)
            break;
        case "fog":
            return gradients.fog(bounds: view)
            break;
        default:
            break;
            
        }
        
        return gradients.defaultDay(bounds: view)
        
    }

    
    /**
     * Function to get the gradient based on the weather
     * - parameter forWeather: the weather
     * - parameter height: the height of the weather cell
     * - return: The correct gradient
     *
     */
    private func getGradient(forWeather: String, height: CGFloat) -> CAGradientLayer{

        switch forWeather {
            case "clear-day":
                
                    return gradients.clearDay(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "clear-night":
                    return gradients.clearNight(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "partly-cloudy-night":
                    return gradients.partlyCloudyNight(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "partly-cloudy-day":
                    return gradients.partlyCloudyDay(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "cloudy":
                    return gradients.cloudy(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "rain":
                    return gradients.rain(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "sleet":
                    return gradients.sleet(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "snow":
                return gradients.snow(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "wind":
                return gradients.wind(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            case "fog":
                return gradients.fog(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
                break;
            default:
                break;
            
            }
        
        return gradients.defaultDay(bounds: CGRect(x: 0, y: 0, width: weatherGradientVw.bounds.width * 2, height: height))
        
        }

}
