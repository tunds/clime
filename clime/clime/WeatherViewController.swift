//
//  ViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 13/12/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces
import PromiseKit
import SVProgressHUD
import MGSwipeTableCell
import AudioToolbox
import AVFoundation
import DZNEmptyDataSet

class WeatherViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    // Expandable tableview cell variables
    internal var dynamicHeight: CGFloat = 0.0
    internal var peak = false
    internal var selectedIndex = -1
    
    // Forecast and location variables
    internal var previewCell: MGSwipeTableCell!
    internal var cityName = ""
    internal var weather = [JSON]()
    internal var forecast : Forecast!
    internal let location = Location()
    internal let config = Config()
    
    // Feedback variables
    internal let progress = Progress()
    internal var refreshSound: AVAudioPlayer!
    
    @IBOutlet weak var forecastTblVw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the Empty tableview
        forecastTblVw.emptyDataSetSource = self
        forecastTblVw.emptyDataSetDelegate = self
        forecastTblVw.tableFooterView = UIView()
      
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if !config.verfiyShakeable() {
            
            getWeatherData()
            
        }
        
        if config.verfiyShakeable() && !config.verfiyShakeableSeen() {
        
            // If it's the first time enabling show the shake view controller
            let shakeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "shakeVC") as! ShakeViewController
            shakeVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            shakeVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(shakeVC, animated: true, completion: nil)
        }
    }
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        // When the user shakes their phone
        if config.verfiyShakeable() {
            
            getWeatherData()
            playRefreshSound()
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    
    private func getWeatherData(){
        
        // Get the upcoming 6 days including current day
        var dates = [Int]()
        for index in 0...5 {
            
            dates.append(Int((Calendar.current.date(byAdding: .day, value: index, to: Date())?.timeIntervalSince1970)!))
            
        }
        
        progress.showProgress(withText: "Please wait a moment...")
        
        switch CLLocationManager.authorizationStatus() {
            
        // If the user has given you access to their location
        case .authorizedAlways, .authorizedWhenInUse:
            
            let currentLocation = location.getActiveLocation()
            // If there's a current location
            if let location = currentLocation.first {
                
                // Store the promises in variables
                forecast = Forecast(lat: location.latitude, long: location.longitude)

                let city = self.location.getCityName(lat: location.latitude, long: location.longitude)
                let upcomingDays = forecast.getForecast(dates: dates)
                
                when(fulfilled: city, upcomingDays).then { city, results -> Void in

                    self.cityName = city
                    self.weather = results
                    
                }.always {
                    
                    self.navigationItem.title = "Weather currently in \(self.cityName)"
                    self.progress.dismissProgress()
                    
                    // Hotfix: To fix the gradient not filling the whole row
                    self.forecastTblVw.reloadData()
                    self.forecastTblVw.setNeedsLayout()
                    self.forecastTblVw.layoutIfNeeded()
                    self.forecastTblVw.reloadData()
                    
                    
                }.catch { error -> Void in
                        
                        self.progress.dismissProgress()
                        Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
                }
                
            } else {
                
                Location().getCurrentPlace().then { place -> Void in
                    
                    Location().saveCoordinates(place: place, active: true)
                    let location = self.location.getActiveLocation().first
                    
                    // Store the promises in variables
                    self.forecast = Forecast(lat: (location?.latitude)!, long: (location?.longitude)!)
                    
                    let city = self.location.getCityName(lat: (location?.latitude)!, long: (location?.longitude)!)
                    let upcomingDays = self.forecast.getForecast(dates: dates)
                    
                    when(fulfilled: city, upcomingDays).then { city, results -> Void in
                        
                        self.cityName = city
                        self.weather = results
                        
                        }.always {
                            
                            self.navigationItem.title = "Weather currently in \(self.cityName)"
                            self.progress.dismissProgress()
                            
                            // Hotfix: To fix the gradient not filling the whole row                            
                            self.forecastTblVw.reloadData()
                            self.forecastTblVw.setNeedsLayout()
                            self.forecastTblVw.layoutIfNeeded()
                            self.forecastTblVw.reloadData()
                            
                            
                        }.catch { error -> Void in
                            
                            self.progress.dismissProgress()
                            Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
                    }
                    
                }
                
            }
            break
        default:
            
            // User has blocked access display this error
            self.progress.dismissProgress()
            Popover(title: "Permission Denied", info: "It seems like you've restricted access for us to use your location, please enable this on your phone via Settings keep scrolling until you see Clime", image: UIImage(named: "warning")).showPopover(view: self)
            
            break
        }
    }
    
    /**
     * Function to play the shake sound
     *
     */
    private func playRefreshSound(){
        
        let file = Bundle.main.url(forResource: "Affirmative", withExtension: "wav")!
        
        do {
            
            refreshSound = try AVAudioPlayer(contentsOf: file)
            refreshSound.prepareToPlay()
            refreshSound.play()
            
        } catch let error as NSError {}
        
    }
    
    
    // MARK: TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Only return 6 rows when the weather has be set
        if !weather.isEmpty {
            
            return 6
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell") as! WeatherTableViewCell
        
        if !weather.isEmpty {
            
            // Setup the peak view for the selected row or else just configure it normally
            if selectedIndex == indexPath.row {
                
                progress.showProgress(withText: "Please wait a moment...")
                
                firstly {
                
                    cell.hideWeather()
                    
                }.then{ _ -> Promise<JSON> in
                    
                    
                    let date = self.weather[indexPath.row]["time"].int!
                    return self.forecast.getHourlyForecast(time: date)
                    
                }.then { result -> Void in
                
                    return cell.configurePeakWeather(readings: result, weather: self.weather[indexPath.row])
                    
                }.then { _ -> Void in
                    
                    // Configure chart
                    cell.configChart()
                
                }.always {

                    self.progress.dismissProgress()
                    
                }.catch { error in
                        
                        self.progress.dismissProgress()
                        Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
                }
                
            } else {
                
                cell.configureWeather(reading: weather[indexPath.row], height: dynamicHeight)
                
            }
            
            // Configure the swipeable share
            let shareAction = MGSwipeButton(title: "Share", backgroundColor: UIColor(red:0.13, green:0.56, blue:0.93, alpha:1.00), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                
                self.performSegue(withIdentifier: "shareSegue", sender: self.weather[indexPath.row])
                return true
            })
            
            // Configure the pull to swipe expansion on the last cell
            shareAction.backgroundColor = UIColor(red:0.60, green:0.69, blue:0.76, alpha:1.00)
            cell.rightButtons = [shareAction]
            cell.rightExpansion.buttonIndex = 0
            cell.rightExpansion.fillOnTrigger = true
            
            // Check to see if it's user's first time
            if config.verfiySeenShare() {
                
                // Open the first cell show the user they can share
                if indexPath.row == 0 {
                    
                    // Add a slight delay
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        
                        cell.showSwipe(.rightToLeft, animated: true, completion: { result in
                            
                            if result {
                                
                                // Set the share as seen
                                self.config.seenShare()
                            }
                        })
                    }
                    
                }
            }
            
        } else {
        
            return UITableViewCell()
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // Set the height for the cell based of the tableviews height
        dynamicHeight = tableView.bounds.height / 6
        
        // For the selected row add extra space for the peak view or else just return a normal cell
        if selectedIndex == indexPath.row {
            
            return (tableView.bounds.height / 6) + 280
            
        } else {
            
            return tableView.bounds.height / 6
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        // If the user selects the same row change the index or else assign a new row
        if selectedIndex == indexPath.row  {
            
            selectedIndex = -1
        } else {
            
            selectedIndex = indexPath.row
        }
        
        
        // Update all the sections in the tableview, this is done so that all the peak cells are resetted rather than reloading the rows, since not all the rows can be reloaded
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
        tableView.endUpdates()
        
        // Only scroll to the row if the index is higher than the selected index
        if selectedIndex != -1 {
            tableView.scrollToRow(at: IndexPath(row: selectedIndex, section: 0), at: .top, animated: true)
        }
    }
    
    /**
     * MARK: SEGUE DELEGATES
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "shareSegue" {
            
            // Configure the view controller
            let shareView = segue.destination as! ShareViewController
            let data = sender as! JSON
            
            shareView.time = data["time"].int!
            shareView.city = cityName
            shareView.icon = data["icon"].string!
            shareView.summary = data["summary"].string!
            shareView.temp = data["apparentTemperature"].int!
        }
        
    }
    
    
    // MARK: DZNEmptyDataSet Delegates
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "warning")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Ooops"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 1.2
        
        let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 20.0), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        let text = "It seems like we're struggling to get your weather data. Please check your internet connection or if you've given us access to your location."
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.lineSpacing = 1.2
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let text = "REFRESH"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16.0), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)

    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        
        let capInsets = UIEdgeInsetsMake(25.0, 25.0, 25.0, 25.0)
        let rectInsets = UIEdgeInsetsMake(-10.0, -20, -10.0,-20)
        return UIImage(named: "button_background")!.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
        
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        getWeatherData()
    }
}

