//
//  SettingsViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 26/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import MessageUI

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var settingTblVw: UITableView!
    @IBOutlet weak var twitterLbl: UILabel!
    @IBOutlet weak var facebookLbl: UILabel!
    @IBOutlet weak var celsiusBtn: UIButton!
    @IBOutlet weak var fahrenheitBtn: UIButton!
    @IBOutlet weak var shakeSwitch: UISwitch!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    internal let twitter = TwitterAuth()
    internal let facebook = FacebookAuth()
    internal let config = Config()
    
    internal let loginColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.00)
    internal let logoutColor = UIColor(red:0.91, green:0.31, blue:0.37, alpha:1.00)
    internal let disabledColor = UIColor(red:0.61, green:0.80, blue:0.99, alpha:1.00)
    internal let activeColor = UIColor(red:0.19, green:0.55, blue:0.93, alpha:1.00)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add the seperator color and fix the extra lines at the bottom of the tableview
        settingTblVw.separatorColor = UIColor(red:0.78, green:0.78, blue:0.80, alpha:0.25)
        settingTblVw.tableFooterView = UIView()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        // Set the switches
        shakeSwitch.setOn(config.verfiyShakeable(), animated: false)
        notificationSwitch.setOn(config.verfiyNotifications(), animated: false)
        
        // Toggle the buttons
        toggleTempBtns()
        
        // Change the twitter and facebook label
        twitter.verify().then { result -> Void in
            
            if result {
                self.twitterLbl.text = "Logout of Twitter"
                self.twitterLbl.textColor = self.logoutColor
                
            } else {
            
                self.twitterLbl.text = "Login to Twitter"
                self.twitterLbl.textColor = self.loginColor
                
            }
        
        }
        
        facebook.verfiy().then { result -> Void in
        
            if result {
                
                self.facebookLbl.text = "Logout of Facebook"
                self.facebookLbl.textColor = self.logoutColor
                
            } else {
                
                self.facebookLbl.text = "Login to Facebook"
                self.facebookLbl.textColor = self.loginColor
                
            }
        }
        
    }
    
    @IBAction func shakeDidChange(_ sender: UISwitch) {
        
        config.toggleShakeable(withStatus: sender.isOn)
    }
    
    @IBAction func notificationDidChange(_ sender: UISwitch) {
        config.toggleNotifications(withValue: sender.isOn)
    }
    
    
    @IBAction func celsiusDidTouch(_ sender: Any) {
        
        config.toggleTempType(withStatus: true)
        toggleTempBtns()
        
    }
    
    @IBAction func fahrenheitDidTouch(_ sender: Any) {
        
        config.toggleTempType(withStatus: false)
        toggleTempBtns()
        
    }
    
    private func toggleTempBtns() {
    
        // Change the color of the temperature buttons depending on state
        if config.verfiyTempType() {
            celsiusBtn.setTitleColor(activeColor, for: .normal)
            fahrenheitBtn.setTitleColor(disabledColor, for: .normal)
            
        } else {
            fahrenheitBtn.setTitleColor(activeColor, for: .normal)
            celsiusBtn.setTitleColor(disabledColor, for: .normal)
            
        }
    }
    
    // MARK: Mail delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    // MARK: Tableview delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch indexPath.section {

        case 1:
            
            switch indexPath.row {
                
            case 0:
                
                facebook.verfiy().then { result -> Void in
                    
                    if result {
                    
                        self.facebook.logout().then { _ -> Void in
                            
                            self.facebookLbl.text = "Login to Facebook"
                            self.facebookLbl.textColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.00)
                        }
                        
                    } else {
                    
                        self.facebook.login(withView: self).then { _ -> Void in
                        
                            self.facebookLbl.text = "Logout of Facebook"
                            self.facebookLbl.textColor = UIColor(red:0.91, green:0.31, blue:0.37, alpha:1.00)
                        }
                    }
                
                }
                
                break
            case 1:
                
                twitter.verify().then { result -> Void in
                    
                    if result {
                    
                        self.twitter.logout().then { _ -> Void in
                        
                            self.twitterLbl.text = "Login to Twitter"
                            self.twitterLbl.textColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.00)
                        }
                        
                    } else {
                    
                        self.twitter.login().then { _ -> Void in
                        
                            self.twitterLbl.text = "Logout of Twitter"
                            self.twitterLbl.textColor = UIColor(red:0.91, green:0.31, blue:0.37, alpha:1.00)
                        }
                    }
                
                }
                
                break
            default:
                break
                
            }
            
            break
        case 2:
            
            switch indexPath.row {
                

            case 1:
                
                let onboardingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "onboardingVC") as! OnboardingViewController
                
                onboardingVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                onboardingVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                self.present(onboardingVC, animated: true, completion: nil)
                
                break
            case 2:
                
                if MFMailComposeViewController.canSendMail() {
                    
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["tundsdev@yahoo.co.uk"])
                    mail.setSubject("Clime - Feedback or possible feature request")
                    present(mail, animated: true, completion: nil)
                    
                } else {
                    
                    Popover(title: "Oops", info: "Something went wrong when trying to send emails", image: UIImage(named: "warning")).showPopover(view: self)
                }
                
                
                break
            default:
                break
                
            }
            
            break
        default:
            break
            
        }
        
        
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        
        if let view = view as? UITableViewHeaderFooterView {
            
            // Background view colors
            view.backgroundView?.backgroundColor = .white
        
            // Text labels
            view.textLabel?.text = view.textLabel?.text?.uppercased()
            view.textLabel?.font = UIFont.init(name: "Lato-Black", size: 11)
            view.textLabel?.backgroundColor = .clear
            view.textLabel?.textColor = UIColor(red:0.16, green:0.54, blue:0.94, alpha:1.00)
        }
    }
}
