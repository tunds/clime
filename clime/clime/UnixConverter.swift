//
//  UnixConverter.swift
//  clime
//
//  Created by Tunde Adegoroye on 06/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

class UnixConverter {

    private var unix: Int!
    private var date: Date!
    private var day: String!
    private var time: String!
    
    init(unix: Int) {

        date = Date(timeIntervalSince1970: TimeInterval(unix))
    }
    
    /**
     * Function to get the day from unix
     */
    public func getCurrentDay() -> String {
        
        // Day formatter
        let dayFormatter = DateFormatter()
        dayFormatter.locale = Locale(identifier: "en_US_POSIX")
        dayFormatter.dateFormat = "EEEE d MMM"
        day = dayFormatter.string(from: date)
        
        return self.day
    }

    
    /**
     * Function to get the day from unix
     */
    public func getDay() -> String {
        
        // Day formatter
        let dayFormatter = DateFormatter()
        dayFormatter.locale = Locale(identifier: "en_US_POSIX")
        dayFormatter.dateFormat = "EEEE"
        day = dayFormatter.string(from: date)
        
        return self.day
    }
    
    public func getDay(withComparison: Date) -> String {
        
        
        // Day formatter
        let dayFormatter = DateFormatter()
        dayFormatter.locale = Locale(identifier: "en_US_POSIX")
        dayFormatter.dateFormat = "EEEE"
        day = dayFormatter.string(from: date)
        let comparisionDay = dayFormatter.string(from: withComparison)
        
        if comparisionDay == day {
            return "The weather for today"
        }
        
        return "The forecast for \(self.day!.lowercased())"
    }
    
    /**
     * Function to get the time from unix
     */
    public func getTime() -> String {
        
        // 12 hour formatter
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "h a"
        timeFormatter.amSymbol = "AM"
        timeFormatter.pmSymbol = "PM"
        time = timeFormatter.string(from: date)
        
        return self.time
    }
    
    /**
     * Function to get the time from unix
     */
    public func get24HourTime() -> Int {
        
        // 12 hour formatter
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "HH"
        time = timeFormatter.string(from: date)
        
        return Int(self.time)!
    }
    
}
