//
//  LoginViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 16/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import PromiseKit
import GooglePlaces
import RealmSwift
import MGSwipeTableCell

class LocationsViewController: UIViewController, GMSAutocompleteResultsViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var locationsTblVw: UITableView!
    
    internal var resultsViewController: GMSAutocompleteResultsViewController?
    internal var searchController: UISearchController?
    internal var textview: UITextView?
    
    internal let progress = Progress()
    internal let location = Location()
    internal let config = Config()
    
    internal var userLocations: Results<Locations>!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Set the color of the seperator & hide empty tableview cells
        locationsTblVw.separatorColor = UIColor(red:0.78, green:0.78, blue:0.80, alpha:0.25)
        locationsTblVw.tableFooterView = UIView()
        
        // Get all the locations
        location.getLocations().then { locations -> Void in
        
            self.userLocations = locations
            self.locationsTblVw.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up the results view controller
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        // Set up the search controller
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // Always use this controller
        definesPresentationContext = true
        
        // Prevent hiding the search bar when searching
        searchController?.hidesNavigationBarDuringPresentation = false
        
    }
    
    // MARK: GOOGLE API DELEGATES

    // Get the users selected location
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        
        // Disable the search controller
        searchController?.isActive = false
        
        self.location.resetAllLocations().then { _ -> Void in
        
            self.location.saveCoordinates(place: place, active: true)
            
        }.then { _ -> Void in
        
            return when(fulfilled: [self.location.saveNewTimeZone(long: place.coordinate.longitude, lat: place.coordinate.latitude),
                                    self.location.saveNewCity(long: place.coordinate.longitude, lat: place.coordinate.latitude)])
            
        }.always {
            
            // Move back to the main screen
            self.tabBarController?.selectedIndex = 0
                
        }.catch { error -> Void in}
 
    }

    // Handle errors
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {}
    
    // MARK: TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if let userLocations = userLocations {
        
            return userLocations.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell") as! LocationTableViewCell
        cell.configureLocationCell(location: userLocations[indexPath.row])
        
        // Configure the swipeable delete
        let deleteAction = MGSwipeButton(title: "Delete", backgroundColor: UIColor(red:1.00, green:0.44, blue:0.35, alpha:1.00), callback: {
            (sender: MGSwipeTableCell!) -> Bool in
            
            firstly {
                
                self.location.deleteLocation(location: self.userLocations[indexPath.row])
                
            }.then { _ -> Void in
                
                self.location.setMostRecentActive()
                
            }.always {
                
                 self.locationsTblVw.reloadData()
                
            }
            
            return true
        })
        
        
        // Configure the pull to swipe expansion on the last cell
        cell.rightButtons = [deleteAction]
        cell.rightExpansion.buttonIndex = 0
        cell.rightExpansion.fillOnTrigger = true
        
        // Check to see if it's user's first time
        if config.verfiySeenDelete() {
            
            // Open the first cell show the user they can share
            if indexPath.row == 0 {
                
                cell.showSwipe(.rightToLeft, animated: true, completion: { result in
                    
                    if result {
                        
                        // Set the share as seen
                        self.config.seenDelete()
                    }
                })
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        firstly {
        
            self.location.resetAllLocations()
            
        }.then { _ -> Void in
            
            return when(fulfilled: [self.location.setActiveLocation(location: self.userLocations[indexPath.row]),
                                   self.location.saveNewTimeZone(long: self.userLocations[indexPath.row]["longitude"] as! Double, lat: self.userLocations[indexPath.row]["latitude"] as! Double),
                                   self.location.saveNewCity(long: self.userLocations[indexPath.row]["longitude"] as! Double, lat: self.userLocations[indexPath.row]["latitude"] as! Double)])
            
        }.always {
             
           self.locationsTblVw.reloadData()
           self.locationsTblVw.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
            
        }.catch { error -> Void in
            
           
            
        }
    }
}

