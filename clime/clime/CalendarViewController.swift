//
//  CalendarViewController.swift
//  clime
//
//  Created by Tunde Adegoroye on 25/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Koyomi
import PromiseKit
import SwiftyJSON
import GooglePlaces
import SVProgressHUD

class CalendarViewController: UIViewController, KoyomiDelegate, UITableViewDataSource, UITableViewDelegate {

    // Configure calender styles
    @IBOutlet weak var calendar: Koyomi! {
        didSet {
            
            // Set up the delegate
            calendar.calendarDelegate = self
            
            // Day styles in the calendar
            calendar.dayPosition = .center
            calendar.selectionMode = .single(style: .background)
            calendar.style = .blue
            
            // Change the text for the weeks
            calendar.weeks = ["S","M","T","W","T","F","S"]
            
            // Set the date format
            calendar.currentDateFormat = "MMMM yyyy"
            
            // Change the background color
            calendar.selectedStyleColor = UIColor(red:0.16, green:0.51, blue:1.00, alpha:1.00)
            
            // Change week color
            calendar.weekColor = UIColor(red:0.72, green:0.75, blue:0.78, alpha:1.00)
            calendar.holidayColor = (UIColor(red:0.49, green:0.53, blue:0.64, alpha:1.00), UIColor(red:0.49, green:0.53, blue:0.64, alpha:1.00))
            calendar.weekdayColor = UIColor(red:0.49, green:0.53, blue:0.64, alpha:1.00)
            
            // Change the font
            calendar.setWeekFont(fontName: "Lato-Regular", size: 10)
                    .setDayFont(fontName: "Lato-Black", size: 10)
            
            // Change the background
            calendar.weekBackgrondColor = .white
            calendar.sectionSeparatorColor = .white
            calendar.separatorColor = .white
            
            // Set the current date
            calendar.select(date: Date())
            
        }
    }
    
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var forecastTblVw: UITableView!
    
    internal var cityName = ""
    internal var weatherIndex: Int?
    
    internal let progress = Progress()
    internal var weather = SwiftyJSON.JSON([])
    internal var forecast: Forecast!
    internal let location = Location()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        getWeatherReading(date: Date()).then { _ -> Void in
            
            // Set the color of the seperator & hide empty tableview cells
            self.forecastTblVw.separatorColor = UIColor(red:0.78, green:0.78, blue:0.80, alpha:0)
            self.forecastTblVw.tableFooterView = UIView()
            self.monthLbl.text = self.calendar.currentDateString(withFormat: "MMMM yyyy")
            
            // Scroll to the current time
            if let _ = self.weather.arrayObject {
                
                var i = 0
                
                for (_, val):(String, JSON) in self.weather {
                    
                    if let time = val["time"].int {
                        
                        // Get the time one hour from now
                        if UnixConverter(unix: Int(Date().timeIntervalSince1970)).get24HourTime() == UnixConverter(unix: time).get24HourTime() {
                            
                            self.forecastTblVw.scrollToRow(at: IndexPath(row: i, section: 0), at: .top, animated: true)
                            break
                        }
                    }
                    
                    i += 1
                }
            }
            
        }
    }
    
    
    @IBAction func previousDidTouch(_ sender: Any) {
        
        calendar.display(in: .previous)
    }
    
    @IBAction func nextDidTouch(_ sender: Any) {
        calendar.display(in: .next)
    }
    
    /**
     * Function to get the weather for a day
     * - parameter date: the day
     * - return: Void
     */
    private func getWeatherReading(date: Date) -> Promise<Void> {
    
        progress.showProgress(withText: "Please wait a moment...")
        
        return Promise { success, fail in
            
            switch CLLocationManager.authorizationStatus() {
                
            case .authorizedWhenInUse, .authorizedAlways:
                
                let currentLocation = location.getActiveLocation()
                
                if let location = currentLocation.first {
                    
                    // Store the promises in variables
                    forecast = Forecast(lat: location.latitude, long: location.longitude)
                    let futureUpcomingForecast = forecast.getHourlyForecast(time: Int(date.timeIntervalSince1970))
                    let city = self.location.getCityName(lat: location.latitude, long: location.longitude)
                    
                    // Make both of the calls
                    when(fulfilled: futureUpcomingForecast, city).then { forecast, city -> Void in
                        
                        self.cityName = city
                        self.weather = forecast
                        
                        }.always {
                            
                            self.navigationItem.title =  "On " + UnixConverter(unix: Int((date.timeIntervalSince1970))).getCurrentDay() + " in " + self.cityName
                            self.forecastTblVw.reloadData()
                            self.progress.dismissProgress()
                            success()
                            
                        }.catch { error in
                            
                            self.progress.dismissProgress()
                            Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
                    }
                    
                } else {
                    
                    Location().getCurrentPlace().then { place -> Void in
                        
                        Location().saveCoordinates(place: place, active: true)
                        let location = self.location.getActiveLocation().first
                        
                        // Store the promises in variables
                        self.forecast = Forecast(lat: (location?.latitude)!, long: (location?.longitude)!)
                        let futureUpcomingForecast = self.forecast.getHourlyForecast(time:
                            Int(date.timeIntervalSince1970))
                        let city = self.location.getCityName(lat: (location?.latitude)!, long: (location?.longitude)!)

                        
                        // Make both of the calls
                        when(fulfilled: futureUpcomingForecast, city).then { forecast, city -> Void in
                            
                            self.cityName = city
                            self.weather = forecast
                            
                            }.always {
                                
                                self.navigationItem.title = "On " + UnixConverter(unix: Int((date.timeIntervalSince1970))).getCurrentDay() + " in " + self.cityName
                                self.forecastTblVw.reloadData()
                                self.progress.dismissProgress()
                                success()
                                
                            }.catch { error in
                                
                                self.progress.dismissProgress()
                                Popover(title: "Something went wrong", info: error.localizedDescription, image: UIImage(named: "warning")).showPopover(view: self)
                        }
                        
                    }
                    
                }
                
                break
            default:
                break
                
            }
            
        }
        
    }
    
    // MARK: KOYOMI DELEGATES
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        
        getWeatherReading(date: date!).then { _ -> Void in
            
            koyomi.select(date: date!)
            
        }.catch { _ in
                
        }
        
    }
    
    // Get the month of the calendar
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        
        monthLbl.text = dateString
    }
    
    // MARK: TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !weather.isEmpty {
        
            return weather.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if !weather.isEmpty {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "readingCell") as! WeatherReadingTableViewCell
            cell.configureWeather(reading: self.weather[indexPath.row], height: 70)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }

}
