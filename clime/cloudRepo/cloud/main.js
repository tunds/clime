
// MARK: Modules
var _ = require('underscore');
var moment_timezone = require('moment-timezone');
const apn = require('apn');

// MARK: Configs
const options = {
    token: {
        key: __dirname + "/public/key.p8",
        keyId: "7MPRRYV9BE",
        teamId: "72T7UWRAFX"
    },
    production: true
};
const forecastApiKey = "a4ff17cd9b8902309106b8801cf48a66/";

/**
 * MARK: Test cloud code function to send notifications
 * Function to simulate push notifications instantly
 * @param {Object} request
 * @param {Object} response
 */
Parse.Cloud.define("dailyForecast", function(request, response) {

    var devices = [];
    var successCount = 0;

    // Get all of the user who want notifications
    var pushQuery = new Parse.Query(Parse.Installation);
    pushQuery.equalTo("notificationsActive", true);

    pushQuery.find({useMasterKey: true}).then(function(results){

      _.each(results, function(result) {

             devices.push({
                 "token" : result.get("deviceToken"),
                 "latitude" : result.get("latitude"),
                 "longitude" : result.get("longitude"),
                 "city" : result.get("timeZoneCity")
             });
      });

      // When all the devices have been collected
      return Parse.Promise.when(devices);

    }).then(function(tokens){

      if (!_.isEmpty(tokens)){

         // Object to hold all of the looped promises
         var promise = Parse.Promise.as();

        _.each(tokens, function(token){

           promise = promise.then(function () {

               var url =  'https://api.darksky.net/forecast/' + forecastApiKey + token["latitude"] + "," + token["longitude"];
               return Parse.Cloud.httpRequest({
                   url: url,
                   params: {
                       'exclude': 'minutely,hourly,daily,flags',
                       'units': 'us',
                       'Content-Type': 'application/json;charset=utf-8'
                   }
               }).then(function(httpResponse) {

                   // Parse and get the fields
                   var weather = JSON.parse(httpResponse.text);
                   var userToken = token["token"];
                   var summary = weather["currently"]["summary"];
                   var city = token["city"];
                   var icon = weather["currently"]["icon"];
                   var temp = weather["currently"]["apparentTemperature"];

                   // Send the notification and increment the position
                    notifyForecast(userToken, summary, city, icon, temp);
                   successCount++;

               }, function(httpResponse) {
                   console.error('Request failed with response code ' + httpResponse.status);
               });
           });

        });

          // When all of the requests have been completed
          return promise;
      }

    }).then(function() {
        response.success("Number of successfull notifications: " + successCount);
    }, function (error) {
        response.error("Error: " + error.code + " " + error.message);
    });
});

/**
 * MARK: Job to send notifications in the morning
 * Cloud code job to send notifications at 7am based on the user's timezone
 * @param {Object} request
 * @param {Object} status
 */
Parse.Cloud.job("morningWeatherNotification", function(request, status) {

    // Get all of the notifications
    var devices = [];
    var successCount = 0;

    // Get all of the user who want notifications
    var pushQuery = new Parse.Query(Parse.Installation);
    pushQuery.equalTo("notificationsActive", true);

    pushQuery.find({useMasterKey: true}).then(function(results){

        _.each(results, function(result) {

            // Get the servers timezone which is in london
            // Convert it using the users timezone
            var serverTime =  moment_timezone().tz("Europe/London");
            var userTime = serverTime.clone().tz(result.get("timeZoneIdentifier")).format("h:mma");

            if ("7:00am" === String(userTime)) {

                    devices.push({
                    "token" : result.get("deviceToken"),
                    "latitude" : result.get("latitude"),
                    "longitude" : result.get("longitude"),
                    "city" : result.get("timeZoneCity")
                });
            }

        });

        // When all the devices have been collected
        return Parse.Promise.when(devices);

    }).then(function(tokens){

        if (!_.isEmpty(tokens)){

            // Object to hold all of the looped promises
            var promise = Parse.Promise.as();

            _.each(tokens, function(token){

                promise = promise.then(function () {

                    return Parse.Cloud.httpRequest({
                        url: 'https://api.darksky.net/forecast/' + forecastApiKey + token["latitude"] + "," + token["longitude"],
                        params: {
                            'exclude': 'minutely,hourly,daily,flags',
                            'units': 'us',
                            'Content-Type': 'application/json;charset=utf-8'
                        }

                    }).then(function(httpResponse) {

                        // Parse and get the fields
                        var weather = JSON.parse(httpResponse.text);
                        var userToken = token["token"];
                        var summary = weather["currently"]["summary"];
                        var city = token["city"];
                        var icon = weather["currently"]["icon"];
                        var temp = weather["currently"]["apparentTemperature"];

                        // Send the notification and increment the position
                        notifyForecast(userToken, summary, city, icon, temp);
                        successCount++;

                    }, function(httpResponse) {
                        console.error('Request failed with response code ' + httpResponse.status);
                    });
                });

            });

            // When all of the requests have been completed
            return promise;
        }

    }).then(function() {
        status.success("Number of successfull notifications: " + successCount);
    }, function (error) {
        status.error("Error: " + error.code + " " + error.message);
    });

});

/**
 * MARK: Hook to remove orphans from the database
 * After delete to delete the remaining user and session data relating to the device token
 * @param {Object} request
 */
Parse.Cloud.afterDelete(Parse.Installation, function(request) {

    // Get the user and the installation id
    var data = JSON.stringify(request.object.get("user"));
    var user = JSON.parse(data);
    var installId = request.object.get("installationId");

    // Query both the User & Session class
    var deleteUserQuery = new Parse.Query(Parse.User);
    deleteUserQuery.equalTo("objectId", user["objectId"]);

    var deleteSessionQuery = new Parse.Query(Parse.Session);
    deleteSessionQuery.equalTo("installationId", installId);

    // Firstly find the user object
    deleteUserQuery.find({
        useMasterKey: true,
        success: function(installs) {

            // Then delete it
            Parse.Object.destroyAll(installs, {
                useMasterKey: true,
                success: function() {

                    // Then find the session object
                    deleteSessionQuery.find({
                        useMasterKey: true,
                        success: function(installs) {

                            // Then delete it
                            Parse.Object.destroyAll(installs, {
                                useMasterKey: true,
                                success: function() {
                                    console.log("Deleted all the orphans");
                                },
                                error: function(error) {
                                    console.error("Error trying to delete related installs " + error.code + ": " + error.message);
                                }

                            });

                        },
                        error: function(error) {
                            console.error("Error trying to find related installs " + error.code + ": " + error.message);
                        }
                    });

                },
                error: function(error) {
                    console.error("Error trying to delete related installs " + error.code + ": " + error.message);
                }

            });
        },
        error: function(error) {
            console.error("Error trying to find related installs " + error.code + ": " + error.message);
        }
    });
});

/**
 * MARK: Functions
 * Helper functions within the class
 */

/**
 * MARK: Function to send a blank notification to a user
 * This takes the params and sends a blank notification
 * @param {String} withToken
 *
 */
function sendBlankNotification(withToken) {

    // Use the const options defined above
    var apnProvider = new apn.Provider(options);

    // Set up the payload notification
    var notification = new apn.Notification();
    notification.topic = "tunds.clime";

    apnProvider.send(notification, withToken).then(function (result) {

        // Only remove devices which return this error code
        if (result.failed[0].status === "410"){

            removeUnregisteredDevices(result.failed[0].device);
        }

    });

    // Close the connection once finished
    apnProvider.shutdown();
}

/**
 * MARK: Function to send the notification to a user
 * This takes the params and sends a notification featuring information about the weather
 * @param {String} withToken
 * @param {String} withSummary
 * @param {String} withCity
 * @param {String} withIcon
 *
 */
function notifyForecast(withToken, withSummary, withCity, withIcon, withTemp) {

    // Use the const options defined above
    var apnProvider = new apn.Provider(options);

    // Set up the payload notification
    var notification = new apn.Notification({
        aps: {
            alert: {
                title : "Good morning!",
                subtitle: "Today's forcast in " + withCity,
                body: withSummary
            },
            "mutable-content" : 1,
            icon: withIcon,
            temp: withTemp
        }
    });

    // Set up the sound, expiry and attach the app using the bundle id
    notification.sound = "Wakeup.wav";
    notification.expiry = Math.floor(Date.now() / 1000) + 14400; // Expires 4 hours from now.
    notification.topic = "tunds.clime";

    apnProvider.send(notification, withToken);

    // Close the connection once finished
    apnProvider.shutdown();
}

/**
 * MARK: Function to remove a device
 * This removes a installation from the database
 * @param {String} withToken
 */
function removeUnregisteredDevices(withToken) {

    var query = new Parse.Query(Parse.Installation);
    query.include("user");
    query.equalTo("deviceToken", withToken);

    query.find({
        useMasterKey: true,
        success: function(installs) {

            _.each(installs, function(install) {

                install.destroy({ useMasterKey: true,
                    success: function() {
                        console.log("Deleted");
                    },
                    error: function(error) {
                        console.error("Error deleting related installs " + error.code + ": " + error.message);
                    }
                });

            });
        },
        error: function(error) {
            console.error("Error finding related installs " + error.code + ": " + error.message);
        }
    });
}

/**
 * MARK: Function to see files in directory
 * Function to see all the files in the node directory (use this for testing)
 * @param {String} dir
 */
var _getAllFilesFromFolder = function(dir) {

    var filesystem = require("fs");
    var results = [];

    filesystem.readdirSync(dir).forEach(function(file) {

        file = dir+'/'+file;
        var stat = filesystem.statSync(file);

        if (stat && stat.isDirectory()) {
            results = results.concat(_getAllFilesFromFolder(file))
        } else results.push(file);

    });

    return results;

};


// var allDevices = [];
// var cleanQuery = new Parse.Query(Parse.Installation);
// cleanQuery.find({useMasterKey: true}).then(function(results){
//
//     _.each(results, function(result) {
//
//         allDevices.push({
//             "token": result.get("deviceToken")
//         });
//
//     });
//
//     // When all the devices have been collected
//     return Parse.Promise.when(allDevices);
//
// }).then(function(tokens){
//
//     if (!_.isEmpty(tokens)){
//
//         var position = 0;
//
//         _.each(tokens, function(token){
//
//             // Send the notification and increment the position
//             sendBlankNotification(token["token"]);
//             position++;
//
//             // Once all the notifications has finished send the response
//             if (position === tokens.length){
//                 console.log("All blank notifications have been sent " + position);
//             }
//         });
//     }
//
// }, function (error) {
//
//     status.error("There was an error with the returned device tokens for the clean up.");
// });
